import numeral from 'numeral'
import validateBTC from 'bitcoin-address-validation'

numeral.register('locale', 'kk', {
  delimiters: {
    thousands: ' ',
    decimal: ','
  }
})

numeral.locale('kk')

const minKaspi = 20000
const maxKaspi = 1000000

export const validateKaspi = amount => {
  const isValid = amount >= minKaspi && amount <= maxKaspi ? true : false

  const kaspiError = isValid ? null : `Сумма должна быть в пределах ${numeral(minKaspi).format()} - ${numeral(maxKaspi).format()} тг.`

  return kaspiError
}

export const validateBTCAddress = addr => {
  const isBTCAddressValid = validateBTC(addr)

  const btcAddressError = isBTCAddressValid && !isBTCAddressValid.testnet ? null : 'Некорректный формат адреса'

  return btcAddressError
}
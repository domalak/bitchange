import orderAPI from './order'
import { userAPI } from './user'
import { adminAPI } from './admin'
import { settingsAPI } from './settings'
import axios from 'axios'
import { host } from '../local'

const api = axios.create({
  baseURL: host
})

export { orderAPI, userAPI, api, adminAPI, settingsAPI }
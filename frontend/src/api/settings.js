import { api } from './'

export const settingsAPI = {
  getHours: () => api.get('/getHours')
}
import { api } from './'

export const adminAPI = {
  adminGetUsers: data => api.get('/adminGetUsers', data),
  adminGetOrders: data => api.get('/adminGetOrders', data)
}
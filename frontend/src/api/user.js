import { api } from './'

export const userAPI = {
  register: data => api.post(`/register`, data),
  login: data => api.post(`/login`, data),
  get: params => api.get(`/user`, {
    params
  }),
  verify: data => api.post('/verification', data),
  verifyPhoto: data => api.post('/upload', data),
  uploadPhotos: data => api.post(`/upload`, data)
}
import { api } from './'

const orderAPI = {
  create: order => api.post(`/order`, order),
  getPrice: params => api.get(`/order-price`, {
    params
  }),
  get: params => api.get(`/order`, {
    params
  }),
  update: order => api.put(`/order`, order),
  setPaymentSent: order => api.post('/set-payment-sent', order),
  getOrdersHistory: data => api.post('/orders-history', data)
}

export default orderAPI
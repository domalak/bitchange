import React, { Component } from 'react'
import { BrowserRouter } from 'react-router-dom'
import Main from './containers/Main'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { sessionActions } from './redux/session/'
import { api } from './api'

export class App extends Component {
  componentDidMount() {
    if (this.props.token) {
      api.defaults.headers.common['Authorization'] = this.props.token
      this.props.setAPIAuth(true)
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.token !== this.props.token) {
      api.defaults.headers.common['Authorization'] = this.props.token
      this.props.setAPIAuth(this.props.token ? true : false)
    }

    if (this.props.socketId && this.props.socketId !== prevProps.socketId) {
      this.props.getUser({ socketId: this.props.socketId })
    }
  }

  render() {
    return (
      <BrowserRouter>
        <Main />
      </BrowserRouter>
    )
  }
}

App.propTypes = {
  token: PropTypes.string,
  getUser: PropTypes.func,
  setAPIAuth: PropTypes.func,
  socketId: PropTypes.string
}

const mapStateToProps = state => ({
  token: state.session.token,
  socketId: state.session.socketId
})

export default connect(mapStateToProps, {
  getUser: sessionActions.get,
  setAPIAuth: sessionActions.setAPIAuth
})(App)

export default {
  receivingPayment: 'receiving_payment',
  userSentPayment: 'user_sent_payment',
  sendingPayment: 'sending_payment',
  complete: 'complete',
  canceled: 'canceled'
}
import React from 'react'
import NumberFormat from 'react-number-format'
import PropTypes from 'prop-types'
import keyIcon from '../assets/icons/key.svg'
import uploadIcon from '../assets/icons/folder-upload.svg'

class Input extends React.Component {
  constructor() {
    super()

    this.inputWrapper = this.inputWrapper.bind(this)
  }

  inputWrapper() {
    switch (this.props.type) {
      case 'number':
        return <div className="field">
          <p className="control has-icons-left">
            <NumberFormat
              value={this.props.value}
              thousandSeparator={' '}
              decimalSeparator={','}
              decimalScale={this.props.decimals}
              suffix={this.props.suffix}
              className={`input`}
              allowNegative={false}
              onValueChange={this.props.onChange}
              onFocus={this.props.onFocus}
              onBlur={this.props.onBlur}
              disabled={this.props.disabled}
            />
            <span className="icon is-small is-left">
              <img src={this.props.img} className={`bc-input-img`} alt="img-error" />
            </span>
          </p>
        </div>
        
      case 'text':
        return <div className="field">
          <p className="control">
            <input
              type="text"
              className="input"
              value={this.props.value}
              onChange={this.props.onChange}
              disabled={this.props.disabled}
            />
          </p>
        </div>
      case 'password':
        return <div className="field">
          <p className="control has-icons-left">
          <input type="password" className="input" value={this.props.value} onChange={this.props.onChange} onBlur={this.props.onBlur} />
            <span className="icon is-small is-left">
              <i className="fas"><img alt="key" src={keyIcon} style={{width: '50%', verticalAlign: 'middle', marginLeft: '25%'}} /></i>
            </span>
          </p>
        </div>
      case 'phone':
        return <div className="field has-addons">
            <div className="control">
              <div className="button is-static">
                +7
              </div>
            </div>
            <div className="control is-expanded">
              <input type="text" className="input" value={this.props.value} onChange={this.props.onChange} onBlur={this.props.onBlur} />
            </div>
          </div>
      case 'smsCode':
        return <div className="field ">
          <div className="control is-expanded">
            <input type="input" className="input" onChange={this.props.onChange}/>
          </div>
        </div>
      case 'photoVerification':
        return <div className="file">
          <label className="file-label">
            <input className="file-input" type="file" name={this.props.name} ref={this.props.ref} onChange={this.props.onChange} />
            <span className="file-cta">
              <span className="file-icon">
                <img src={uploadIcon} alt="upload" />
              </span>
              <span className="file-label">
                {this.props.fileName ? this.props.fileName : 'Выберите фото'}
              </span>
            </span>
          </label>
        </div>
      default:
        return null
    }
  }

  render() {
    return <div className={`field is-horizontal bc-field`}>
      <div className="field-label" style={{ flexGrow: 2 }}>
        <label className="label">{this.props.label}</label>
      </div>

      <div className="field-body">
        <div className="field">
          {this.inputWrapper()}

          <p className="help is-danger">{this.props.error}</p>
          <p className="help">{this.props.littleInfo} </p>
        </div>
          
        
      </div>
    </div>
  }
}

Input.propTypes = {
  label: PropTypes.string,
  value: PropTypes.any,
  img: PropTypes.string,
  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  type: PropTypes.string,
  decimals: PropTypes.number,
  suffix: PropTypes.string,
  phone: PropTypes.string,
  password: PropTypes.string,
  error: PropTypes.string,
  ref: PropTypes.func,
  fileName: PropTypes.string,
  name: PropTypes.string,
  littleInfo: PropTypes.string,
  disabled: PropTypes.bool,
}

export default Input
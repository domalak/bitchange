/* eslint-env jest */

import React from 'react'
import FormButton from './FormButton'
import renderer from 'react-test-renderer'

it('renders without crashing', () => {
  const component = renderer.create(
    <FormButton />
  )
  expect(component.toJSON()).toMatchSnapshot()
})

/* eslint-env jest */

import React from 'react'
import Input from './Input'
import renderer from 'react-test-renderer'

it('renders without crashing', () => {
  const component = renderer.create(
    <Input />
  )
  expect(component.toJSON()).toMatchSnapshot()
})

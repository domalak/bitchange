import React from 'react'
import PropTypes from 'prop-types'

class FormButton extends React.Component {
  render() {
    return <div className="field is-horizontal">
      <div className="field-label" style={{ flexGrow: 2 }}>
      </div>
      <div className="field-body">
        <div className={`field is-grouped`}>
          <div className="control">
            <button className="button is-primary" onClick={this.props.onClick} disabled={this.props.disabled}>
              {this.props.label}
            </button>
          </div>
          <div className="control">
            <button className="button is-white has-text-danger">
              {this.props.error}
            </button>
          </div>
        </div>
          
      </div>
      
    </div>
  }
}

FormButton.propTypes = {
  onClick: PropTypes.func,
  onSubmit: PropTypes.func,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.string
}

export default FormButton
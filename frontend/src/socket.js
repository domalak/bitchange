import io from 'socket.io-client'
import { socketHost } from './local'

const socket = io(socketHost)

export default socket
/* eslint-env jest */

import React from 'react'
import { App } from './App'
// import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

it('renders without crashing', () => {
  const wrapper = shallow(<App />)

  expect(wrapper.debug()).toMatchSnapshot()
})

import React, { Component } from 'react'
import { Line } from 'react-chartjs-2'
import NumberFormat from 'react-number-format'
import Form from './Form'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

export class KaspiBTC extends Component {
  render() {
    const price = this.props.price
    const data = {
      labels: this.props.dates,
      datasets: [{
        label: `BTC (Binance ${this.props.percent > 0 ? '+' : '-'} ${Math.abs(this.props.percent)}%)`,
        data: this.props.rates,
        fill: false,
        borderColor: 'blue',
        pointRadius: 0,
        borderWidth: 1,
        pointHitRadius: 8
      }]
    }
    const options = {
      tooltips: {
        callbacks: {
          label: function (tooltipItem) {
            let label = data.datasets[tooltipItem.datasetIndex].label || ''

            if (label) {
              label += ': '
            }
            label += Math.round(tooltipItem.yLabel * 100) / 100
            return label
          }
        }
      }
    }

    return (
      <div className={`box bc-box`}>
        <div className="columns">
          <div className="column">
            <Form />
          </div>
          <div className={`column has-text-right-tablet bc-right-column`}>
            <h1 className="title is-4">Курс BTC</h1>
            <h2 className="subtitle is-5">
              <NumberFormat value={price} thousandSeparator={' '} decimalSeparator={','} decimalScale={2} fixedDecimalScale={true} displayType={'text'} suffix={' тг.'} />
            </h2>
            <Line data={data} options={options} />
          </div>
        </div>

      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    price: state.kaspiBTC.price,
    dates: state.kaspiBTC.dates,
    rates: state.kaspiBTC.rates,
    percent: state.kaspiBTC.percent
  }
}

KaspiBTC.propTypes = {
  price: PropTypes.number,
  dates: PropTypes.array,
  rates: PropTypes.array,
  percent: PropTypes.number
}

export default connect(mapStateToProps)(KaspiBTC)
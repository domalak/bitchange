import React, { Component } from 'react'
import kaspiImg from '../../assets/images/kaspi.png'
import bitcoinImg from '../../assets/images/bitcoin.png'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { kaspiBTCActions } from '../../redux/kaspiBTC'
import { orderActions } from '../../redux/order'
import { withRouter } from 'react-router-dom'
import { Input, FormButton } from '../../components'
import { validateKaspi } from '../../helpers/validations'

export class Form extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isKaspiValid: true,
      isFormValid: true,
      formError: null
    }

    this.updateBTC = this.updateBTC.bind(this)
    this.updateKZT = this.updateKZT.bind(this)
    this.onKZTFocus = this.onKZTFocus.bind(this)
    this.onKZTBlur = this.onKZTBlur.bind(this)
    this.onBTCFocus = this.onBTCFocus.bind(this)
    this.onBTCBlur = this.onBTCBlur.bind(this)
    this.createNewOrder = this.createNewOrder.bind(this)
  }

  updateBTC(values) {
    if (this.props.btcFocused) {
      this.props.updateBTC(values.floatValue)
    }
  }

  updateKZT(values) {
    if (this.props.kztFocused) {
      this.props.updateKZT(values.floatValue)
    }
  }

  onKZTFocus() {
    this.props.setFocusKZT(true)
  }

  onKZTBlur() {
    this.props.setFocusKZT(false)
  }

  onBTCFocus() {
    this.props.setFocusBTC(true)
  }

  onBTCBlur() {
    this.props.setFocusBTC(false)
  }

  createNewOrder() {
    const { amountKZT, amountBTC, price } = this.props
    const type = 'kaspibtc'

    this.props.createNewOrder({ amountKZT, amountBTC, price, type })
  }

  validateKaspi() {
    const kaspiError = validateKaspi(this.props.amountKZT)

    const isKaspiValid = kaspiError ? false : true

    if (isKaspiValid !== this.state.isKaspiValid) {
      this.setState(state => ({ ...state, isKaspiValid, kaspiError }))
    }
  }

  validateForm() {
    const isFormValid = this.props.workingHours && this.state.isKaspiValid ? true : false
    const formError = this.props.workingHours ? null : 'Доступно с 10:00 по 22:00'

    this.setState({ isFormValid, formError })
  }

  componentDidMount() {
    this.validateKaspi()
    this.validateForm()    
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.amountKZT !== prevProps.amountKZT) {
      this.validateKaspi()
    }

    if (this.props.workingHours !== prevProps.workingHours || this.state.isKaspiValid !== prevState.isKaspiValid) {
      this.validateForm()
    }
  }

  render() {
    const amountKZT = this.props.amountKZT
    const amountBTC = this.props.amountBTC

    return (
      <div>
        <h1 className={`title is-4`}>Обмен Kaspi <span role="img" aria-label="sheep">👉</span> BTC</h1>
        <h2 className={`subtitle is-5 bc-subtitle`}>Обменяйте свои Kaspi KZT на BTC всего за несколько минут!</h2>

        <Input
          type="number"
          img={kaspiImg}
          label={`Отправляете`}
          value={amountKZT}
          decimals={0}
          suffix=" тг."
          onChange={this.updateKZT}
          onFocus={this.onKZTFocus}
          onBlur={this.onKZTBlur}
          error={this.state.kaspiError}
        />

        <Input
          type="number"
          img={bitcoinImg}
          label={`Получаете`}
          value={amountBTC}
          decimals={5}
          suffix=" BTC"
          onChange={this.updateBTC}
          onFocus={this.onBTCFocus}
          onBlur={this.onBTCBlur}
        />

        <FormButton
          disabled={!this.state.isFormValid}
          label="Далее"
          onClick={this.createNewOrder}
          error={this.state.formError}
        />
      </div>
    )
  }
}

Form.propTypes = {
  price: PropTypes.number,
  amountKZT: PropTypes.number,
  amountBTC: PropTypes.number,
  kztFocused: PropTypes.bool,
  btcFocused: PropTypes.bool,
  updateKZT: PropTypes.func,
  updateBTC: PropTypes.func,
  setFocusKZT: PropTypes.func,
  setFocusBTC: PropTypes.func,
  createNewOrder: PropTypes.func,
  history: PropTypes.object,
  order: PropTypes.object,
  workingHours: PropTypes.bool
}

const mapStateToProps = state => ({
  price: state.kaspiBTC.price,
  amountKZT: state.kaspiBTC.amountKZT,
  amountBTC: state.kaspiBTC.amountBTC,
  kztFocused: state.kaspiBTC.kztFocused,
  btcFocused: state.kaspiBTC.btcFocused,
  order: state.order,
  workingHours: state.settings.workingHours
})

export default withRouter(connect(mapStateToProps, {
  updateKZT: kaspiBTCActions.updateKZT,
  updateBTC: kaspiBTCActions.updateBTC,
  setFocusKZT: kaspiBTCActions.setFocusKZT,
  setFocusBTC: kaspiBTCActions.setFocusBTC,
  createNewOrder: orderActions.create
})(Form))
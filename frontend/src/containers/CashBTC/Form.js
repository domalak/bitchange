import React, { Component } from 'react'
import cashImg from '../../assets/images/cash.png'
import bitcoinImg from '../../assets/images/bitcoin.png'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { cashBTCActions } from '../../redux/cashBTC'
import { orderActions } from '../../redux/order'
import { withRouter } from 'react-router-dom'
import { Input, FormButton } from '../../components'

export class Form extends Component {
  constructor(props) {
    super(props)

    this.updateBTC = this.updateBTC.bind(this)
    this.updateKZT = this.updateKZT.bind(this)
    this.onKZTFocus = this.onKZTFocus.bind(this)
    this.onKZTBlur = this.onKZTBlur.bind(this)
    this.onBTCFocus = this.onBTCFocus.bind(this)
    this.onBTCBlur = this.onBTCBlur.bind(this)
    this.createNewOrder = this.createNewOrder.bind(this)
  }

  updateBTC(values) {
    if (this.props.btcFocused) {
      this.props.updateBTC(values.floatValue)
    }
  }

  updateKZT(values) {
    if (this.props.kztFocused) {
      this.props.updateKZT(values.floatValue)
    }
  }

  onKZTFocus() {
    this.props.setFocusKZT(true)
  }

  onKZTBlur() {
    this.props.setFocusKZT(false)
  }

  onBTCFocus() {
    this.props.setFocusBTC(true)
  }

  onBTCBlur() {
    this.props.setFocusBTC(false)
  }

  createNewOrder() {
    const { amountKZT, amountBTC, price } = this.props
    const type = 'cashbtc'

    this.props.createNewOrder({ amountKZT, amountBTC, price, type })

    this.props.history.push('/new-order')
  }

  render() {
    const amountKZT = this.props.amountKZT
    const amountBTC = this.props.amountBTC

    return (
      <div>
        <h1 className={`title is-4`}>Обмен Наличные <span role="img" aria-label="sheep">👉</span> BTC</h1>
        <h2 className={`subtitle is-5 bc-subtitle`}>Обменяйте свои наличные KZT на BTC</h2>

        <Input
          type="number"
          img={cashImg}
          label={`Отдаете`}
          value={amountKZT}
          decimals={0}
          suffix=" тг."
          onChange={this.updateKZT}
          onFocus={this.onKZTFocus}
          onBlur={this.onKZTBlur}
        />

        <Input
          type="number"
          img={bitcoinImg}
          label={`Получаете`}
          value={amountBTC}
          decimals={5}
          suffix=" BTC"
          onChange={this.updateBTC}
          onFocus={this.onBTCFocus}
          onBlur={this.onBTCBlur}
        />

        <FormButton label="Далее" onClick={this.createNewOrder} />
      </div>
    )
  }
}

Form.propTypes = {
  price: PropTypes.number,
  amountKZT: PropTypes.number,
  amountBTC: PropTypes.number,
  kztFocused: PropTypes.bool,
  btcFocused: PropTypes.bool,
  updateKZT: PropTypes.func,
  updateBTC: PropTypes.func,
  setFocusKZT: PropTypes.func,
  setFocusBTC: PropTypes.func,
  createNewOrder: PropTypes.func,
  history: PropTypes.object
}

const mapStateToProps = state => ({
  price: state.cashBTC.price,
  amountKZT: state.cashBTC.amountKZT,
  amountBTC: state.cashBTC.amountBTC,
  kztFocused: state.cashBTC.kztFocused,
  btcFocused: state.cashBTC.btcFocused
})

export default withRouter(connect(mapStateToProps, {
  updateKZT: cashBTCActions.updateKZT,
  updateBTC: cashBTCActions.updateBTC,
  setFocusKZT: cashBTCActions.setFocusKZT,
  setFocusBTC: cashBTCActions.setFocusBTC,
  createNewOrder: orderActions.create
})(Form))
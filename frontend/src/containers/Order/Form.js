import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { orderActions } from '../../redux/order'
import DynamicFields from './DynamicFields'
import { withRouter } from 'react-router-dom'
import constants from '../../constants'

export class Form extends Component {
  render() {
    let orderStatus

    switch (this.props.status) {
      case constants.receivingPayment:
        orderStatus = 'Ожидается оплата заказа пользователем'
        break
      case constants.userSentPayment:
        orderStatus = 'Пользователь сделал перевод'
        break
      case constants.sendingPayment:
        orderStatus = 'Ожидается отправка средств пользователю'
        break
      case constants.complete:
        orderStatus = 'Завершено'
        break
      case constants.canceled:
        orderStatus = 'Отменен'
        break
      default:
        break
    }

    return (
      <div>
        <h1 className={`title is-4`}>Заказ №{this.props.orderNumber}</h1>

        <DynamicFields />

        <div className={`field is-horizontal bc-field`}>
          <div className="field-label" style={{ flexGrow: 2 }}>
            <label className="label">Статус</label>
          </div>

          <div className="field-body">
            <div className="field">
              <p className="control">
                {orderStatus}
              </p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Form.propTypes = {
  setPaymentSent: PropTypes.func,
  status: PropTypes.string,
  orderNumber: PropTypes.number
}

const mapStateToProps = state => ({
  status: state.order.status,
  orderNumber: state.order.orderNumber
})

export default withRouter(connect(mapStateToProps, {
  setPaymentSent: orderActions.setPaymentSent
})(Form))
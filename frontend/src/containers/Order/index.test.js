/* eslint-env jest */

import React from 'react'
import { Order } from './index'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

test('renders without crashing', () => {
  const match = {
    params: {
      id: 1
    }
  }

  const component = shallow(
    <Order match={match} />
  )
  expect(component.debug()).toMatchSnapshot()
})

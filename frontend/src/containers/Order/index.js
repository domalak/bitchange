import React, { Component } from 'react'
import NumberFormat from 'react-number-format'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import clockIcon from '../../assets/icons/clock.svg'
import { orderActions } from '../../redux/order'
import Form from './Form'
import socket from '../../socket'
import constants from '../../constants'
import { FormButton } from '../../components'

const priceTimeout = 15 * 60 * 1000

function timeToStr(time) {
  const minutes = time > 0 ? Math.floor(time / 60000) : 0
  const seconds = Math.abs(Math.floor((time % 60000) / 1000))
  const secondsStr = seconds < 10 ? '0' + seconds : seconds

  return minutes + ':' + secondsStr
}

export class Order extends Component {
  constructor(props) {
    super(props)

    this.state = {
      timeLeft: priceTimeout,
      timeLeftStr: '15:00'
    }

    this.countdown = this.countdown.bind(this)
    this.setPaymentSent = this.setPaymentSent.bind(this)
  }

  componentDidMount() {
    if (!this.props.match.params.id) {
      this.props.history.push('/orders-history')
    }

    if (this.props.apiSet) {
      this.props.getOrder({
        orderNumber: this.props.match.params.id
      })

      if (this.props.status === constants.receivingPayment) {
        this.countdown()
      }
    }

    if (this.props.status === constants.receivingPayment) {
      this.setState(state => ({
        ...state,
        interval: setInterval(() => {
          this.countdown()
        }, 1000)
      }))
    }

    socket.on('order', res => {
      this.props.updateOrderStatus(res)
    })
  }

  componentDidUpdate(prevProps) {
    if (this.props.apiSet !== prevProps.apiSet && this.props.apiSet) {
      this.props.getOrder({
        orderNumber: this.props.match.params.id
      })
    }
  }

  componentWillUnmount() {
    clearInterval(this.state.interval)
  }

  countdown() {
    const date = this.props.date ? new Date(this.props.date) : new Date()
    const timeLeft = priceTimeout - Date.now() + date.getTime()
    const orderNumber = this.props.match.params.id

    if (timeLeft < 0) {
      return this.props.getOrder({
        orderNumber
      })
    }

    this.setState({ timeLeft, timeLeftStr: timeToStr(timeLeft) })
  }

  dynamicInstructions() {
    if (this.props.status === constants.receivingPayment) {
      if (this.props.type === 'kaspibtc') {
        return <div>
          <p>
            <span>{this.state.timeLeftStr}</span>
            <span className="image is-24x24 bc-clock">
              <img src={clockIcon} alt="clock" />
            </span>
            <br /><br />
            Просим Вас сделать перевод на карту Kaspi, указанную ниже, в течение 15 минут. По истечении этого времени цена может обновиться.
            <br /><br />
            Карта Kaspi:
        </p>
          <div className="notification">
            5169 4971 0139 0270
        </div>
        </div>
      }
      if (this.props.type === 'btckaspi') {
        return <div>
          <p>
            <span style={{ marginRight: 10 + 'px' }}>{this.state.timeLeftStr}</span>
            <span className="image is-24x24 bc-clock">
              <img src={clockIcon} alt="clock" />
            </span>
            <br /><br />
            Просим Вас сделать перевод на адрес кошелька Bitcoin, указанный ниже, в течение 15 минут. По истечении этого времени цена может обновиться.
            <br /><br />
            Адрес Bitcoin:
        </p>
        <div className="notification">
          bc1qdlp4sz3rpvwaf38k994umpvr3plyp7qmac6pjx
        </div>
        </div>
      }
    }

    return null
  }

  setPaymentSent() {
    this.props.setPaymentSent({ orderNumber: this.props.orderNumber })
  }

  dynamicFormButton() {
    if (this.props.status === constants.receivingPayment) {
      if (this.props.type === 'kaspibtc') {
        return <FormButton label="Перевод на Kaspi отправлен" onClick={this.setPaymentSent} />
      }
      if (this.props.type === 'btckaspi') {
        return <FormButton label="Перевод на Bitcoin отправлен" onClick={this.setPaymentSent} />
      }
    }

    return null
  }

  render() {
    const price = this.props.price

    return (
      <section className="section">
        <div className="container">
          <div className="box bc-box">
            <div className="columns">
              <div className="column">
                <Form />
              </div>

              <div className="column has-text-right-tablet bc-right-column">
                <h1 className="title is-4">Стоимость BTC</h1>
                <h2 className="subtitle is-5">
                  <NumberFormat value={price} thousandSeparator={' '} decimalSeparator={','} decimalScale={2} fixedDecimalScale={true} displayType={'text'} suffix={' тг.'} />
                </h2>

                {this.dynamicInstructions()}
                <br /><br />
                {this.dynamicFormButton()}
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

Order.propTypes = {
  price: PropTypes.number,
  type: PropTypes.string,
  date: PropTypes.string,
  getOrder: PropTypes.func,
  history: PropTypes.object,
  status: PropTypes.string,
  apiSet: PropTypes.bool,
  match: PropTypes.object,
  updateOrderStatus: PropTypes.func,
  orderNumber: PropTypes.number,
  setPaymentSent: PropTypes.func
}

const mapStateToProps = state => ({
  price: state.order.price,
  type: state.order.type,
  date: state.order.date,
  status: state.order.status,
  apiSet: state.session.apiSet,
  orderNumber: state.order.orderNumber
})

export default withRouter(connect(mapStateToProps, {
  getOrder: orderActions.get,
  updateOrderStatus: orderActions.updateStatus,
  setPaymentSent: orderActions.setPaymentSent
})(Order))
import React, { Component } from 'react'
import kaspiImg from '../../assets/images/kaspi.png'
import bitcoinImg from '../../assets/images/bitcoin.png'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { btcKaspiActions } from '../../redux/btcKaspi'
import { withRouter } from 'react-router-dom'
import { orderActions } from '../../redux/order'
import Input from '../../components/Input'
import FormButton from '../../components/FormButton'
import { validateKaspi } from '../../helpers/validations'

export class Form extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isKaspiValid: true,
      isFormValid: true,
      formError: null
    }

    this.updateBTC = this.updateBTC.bind(this)
    this.updateKZT = this.updateKZT.bind(this)
    this.onKZTFocus = this.onKZTFocus.bind(this)
    this.onKZTBlur = this.onKZTBlur.bind(this)
    this.onBTCFocus = this.onBTCFocus.bind(this)
    this.onBTCBlur = this.onBTCBlur.bind(this)
    this.createNewOrder = this.createNewOrder.bind(this)
  }

  updateBTC(values) {
    if (this.props.btcFocused) {
      this.props.updateBTC(values.floatValue)
    }
  }

  updateKZT(values) {
    if (this.props.kztFocused) {
      this.props.updateKZT(values.floatValue)
    }
  }

  onKZTFocus() {
    this.props.setFocusKZT(true)
  }

  onKZTBlur() {
    this.props.setFocusKZT(false)
  }

  onBTCFocus() {
    this.props.setFocusBTC(true)
  }

  onBTCBlur() {
    this.props.setFocusBTC(false)
  }

  createNewOrder() {
    const { amountKZT, amountBTC, price } = this.props
    const type = 'btckaspi'

    this.props.createNewOrder({ amountKZT, amountBTC, price, type })

    this.props.history.push('/new-order')
  }

  validateKaspi() {
    const kaspiError = validateKaspi(this.props.amountKZT)

    const isKaspiValid = kaspiError ? false : true

    if (isKaspiValid !== this.state.isKaspiValid) {
      this.setState(state => ({ ...state, isKaspiValid, kaspiError }))
    }
  }

  validateForm() {
    const isFormValid = this.props.workingHours && this.state.isKaspiValid ? true : false
    const formError = this.props.workingHours ? null : 'Доступно с 10:00 по 22:00'

    this.setState({ isFormValid, formError })
  }

  componentDidMount() {
    this.validateKaspi()
    this.validateForm()
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.amountKZT !== prevProps.amountKZT) {
      this.validateKaspi()
    }

    if (this.props.workingHours !== prevProps.workingHours || this.state.isKaspiValid !== prevState.isKaspiValid) {
      this.validateForm()
    }
  }

  render() {
    const amountKZT = this.props.amountKZT
    const amountBTC = this.props.amountBTC

    return (
      <div>
        <h1 className={`title is-4`}>Обмен BTC <span role="img" aria-label="point-right">👉</span> Kaspi</h1>
        <h2 className={`subtitle is-5 bc-subtitle`}>Обменяйте свои Kaspi KZT на BTC всего за несколько минут!</h2>

        <Input
          type="number"
          img={bitcoinImg}
          label={`Отправляете`}
          value={amountBTC}
          decimals={5}
          suffix=" BTC"
          onChange={this.updateBTC}
          onFocus={this.onBTCFocus}
          onBlur={this.onBTCBlur}
        />

        <Input
          type="number"
          img={kaspiImg}
          label={`Получаете`}
          value={amountKZT}
          decimals={0}
          suffix=" тг."
          onChange={this.updateKZT}
          onFocus={this.onKZTFocus}
          onBlur={this.onKZTBlur}
          error={this.state.kaspiError}
        />

        <FormButton
          label="Далее"
          onClick={this.createNewOrder}
          disabled={!this.state.isFormValid}
          error={this.state.formError}
        />
      </div>
    )
  }
}

Form.propTypes = {
  price: PropTypes.number,
  amountKZT: PropTypes.number,
  amountBTC: PropTypes.number,
  kztFocused: PropTypes.bool,
  btcFocused: PropTypes.bool,
  updateKZT: PropTypes.func,
  updateBTC: PropTypes.func,
  setFocusKZT: PropTypes.func,
  setFocusBTC: PropTypes.func,
  createNewOrder: PropTypes.func,
  history: PropTypes.object,
  workingHours: PropTypes.bool
}

const mapStateToProps = state => ({
  price: state.btcKaspi.price,
  amountKZT: state.btcKaspi.amountKZT,
  amountBTC: state.btcKaspi.amountBTC,
  kztFocused: state.btcKaspi.kztFocused,
  btcFocused: state.btcKaspi.btcFocused,
  workingHours: state.settings.workingHours
})

export default withRouter(connect(mapStateToProps, {
  updateKZT: btcKaspiActions.updateKZT,
  updateBTC: btcKaspiActions.updateBTC,
  setFocusKZT: btcKaspiActions.setFocusKZT,
  setFocusBTC: btcKaspiActions.setFocusBTC,
  createNewOrder: orderActions.create
})(Form))
/* eslint-env jest */

import React from 'react'
import { BTCKaspi } from './index'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

it('renders without crashing', () => {
  const component = shallow(
    <BTCKaspi />
  )
  expect(component.debug()).toMatchSnapshot()
})

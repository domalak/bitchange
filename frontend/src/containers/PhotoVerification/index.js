import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import Input from '../../components/Input'
import FormButton from '../../components/FormButton'
import { sessionActions } from '../../redux/session'

export class PhotoVerification extends Component {
  constructor() {
    super()
    this.state = {
      idPhoto: null,
      selfPhoto: null,
      isValid: false
    }
    this.OnPhotoVerify = this.OnPhotoVerify.bind(this)
    this.OnIdPhotoCheck = this.OnIdPhotoCheck.bind(this)
    this.OnSelfPhotoCheck = this.OnSelfPhotoCheck.bind(this)
    this.onOptionalCheck = this.onOptionalCheck.bind(this)
    this.isValidCheck = this.isValidCheck.bind(this)
  }

  isValidCheck() {
    if (this.state.idPhoto && this.state.selfPhoto) {
      return this.setState({ ...this.state, isValid: true })
    }

    this.setState({ ...this.state, isValid: false })
  }

  OnPhotoVerify() {
    const data = new FormData()
    data.append('phone', this.props.phone)
    data.append('files[]', this.state.idPhoto, `${this.props.phone}_idPhoto`)
    data.append('files[]', this.state.selfPhoto, `${this.props.phone}_selfPhoto`)
    if (this.state.optionalPhoto) {
      data.append('files[]', this.state.optionalPhoto, `${this.props.phone}_idBackPhoto`)
    }

    this.props.uploadPhotos(data)
  }

  OnIdPhotoCheck(e) {
    e.preventDefault()
    this.setState({
      idPhoto: e.target.files[0]
    })
  }

  OnSelfPhotoCheck(e) {
    e.preventDefault()
    this.setState({
      selfPhoto: e.target.files[0]
    })
  }

  onOptionalCheck(e) {
    e.preventDefault()
    this.setState({
      optionalPhoto: e.target.files[0]
    })
  }

  componentDidMount() {
    if (this.props.phoneVerified && this.props.photoVerifyStatus === 'accepted') {
      const route = this.props.orderNumber ? '/new-order' : '/'
      this.props.history.push(route)
    }
  }

  componentDidUpdate() {
    if (this.props.phoneVerified && this.props.photoVerifyStatus === 'accepted') {
      const route = this.props.orderNumber ? '/new-order' : '/'
      this.props.history.push(route)
    }
  }


  render() {
    const sendingPhoto = () => {
      switch (this.props.photoVerifyStatus) {
        case 'processing':
          return <div className="notification is-primary">
            На данный момент Ваш аккаунт находится в <strong>статусе рассмотрения</strong>, как только оператор проверит Ваши фотографии, все транзакции снова станут доступны.
        </div>

        case 'denied':
          return <div>
            <div className="notification is-warning">
              Ваши фотографии были отклонены. <strong>Причина:</strong> {this.props.photoDenyReason}.
            </div>

            <h2 className={`subtitle is-5 bc-subtitle`}>Пожалуйста, прикрепите свою фотографию вместе со сканом своего удостоверения.</h2>
            <Input
              type="photoVerification"
              label={`Фото удостоверения личности`}
              onChange={this.OnIdPhotoCheck}
              fileName={this.state.idPhoto ? this.state.idPhoto.name : null}
              name="file"
            />

            <Input
              type="photoVerification"
              label={`Фото обратной стороны удостоверения личности`}
              onChange={this.onOptionalCheck}
              fileName={this.state.optionalPhoto ? this.state.optionalPhoto.name : null}
              name="file"
              littleInfo="Опционально"
            />

            <Input
              type="photoVerification"
              label={`Ваше фото вместе с удостоверением личности`}
              onChange={this.OnSelfPhotoCheck}
              fileName={this.state.selfPhoto ? this.state.selfPhoto.name : null}
              name="file"
            />
            <div className="field-label">
            </div>
            <FormButton disabled={!(this.state.idPhoto && this.state.selfPhoto)} label="Далее" onClick={this.OnPhotoVerify} />
          </div>
        default:
          return <div>
            <h2 className={`subtitle is-5 bc-subtitle`}>Пожалуйста, прикрепите фотографии удостоверения личности/паспорта и себя с удостоверением/паспортом в руке. Убедитесь, что на фотографиях все четко видно.</h2>
            <Input
              type="photoVerification"
              label={`Фото удостоверения личности`}
              onChange={this.OnIdPhotoCheck}
              fileName={this.state.idPhoto ? this.state.idPhoto.name : null}
              name="file"
            />

            <Input
              type="photoVerification"
              label={`Фото обратной стороны удостоверения личности`}
              onChange={this.onOptionalCheck}
              fileName={this.state.optionalPhoto ? this.state.optionalPhoto.name : null}
              name="file"
              littleInfo="Опционально"
            />

            <Input
              type="photoVerification"
              label={`Ваше фото вместе с удостоверением личности`}
              onChange={this.OnSelfPhotoCheck}
              fileName={this.state.selfPhoto ? this.state.selfPhoto.name : null}
              name="file"
            />
            <div className="field-label">
            </div>
            <FormButton disabled={!(this.state.idPhoto && this.state.selfPhoto)} label="Далее" onClick={this.OnPhotoVerify} />
          </div>
      }
    }

    return (
      <section className="section">
        <div className="container">
          <div className="box bc-box">
            <div className="columns">
              <div className="column">
                <h1 className={`title is-4`}>Подтверждение Вашей личности</h1>
                {sendingPhoto()}
              </div>

            </div>

          </div>

        </div>

      </section>
    )
  }
}

PhotoVerification.propTypes = {
  phoneVerified: PropTypes.bool,
  history: PropTypes.object,
  orderNumber: PropTypes.number,
  phone: PropTypes.string,
  photoVerifyStatus: PropTypes.string,
  uploadPhotos: PropTypes.func,
  photoDenyReason: PropTypes.string
}

const mapStateToProps = state => ({
  phoneVerified: state.session.phoneVerified,
  orderNumber: state.order.orderNumber,
  phone: state.session.phone,
  photoVerifyStatus: state.session.photoVerifyStatus,
  photoDenyReason: state.session.photoDenyReason
})

export default withRouter(connect(mapStateToProps, {
  uploadPhotos: sessionActions.uploadPhotos
})(PhotoVerification))
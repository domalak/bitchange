import React, { Component } from 'react'
import NumberFormat from 'react-number-format'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import clockIcon from '../../assets/icons/clock.svg'
import { orderActions } from '../../redux/order'
import Form from './Form'
import constants from '../../constants'

const priceTimeout = 3 * 60 * 1000

function timeToStr(time) {
  const minutes = time > 0 ? Math.floor(time / 60000) : 0
  const seconds = Math.abs(Math.floor((time % 60000) / 1000))
  const secondsStr = seconds < 10 ? '0' + seconds : seconds

  return minutes + ':' + secondsStr
}

export class NewOrder extends Component {
  constructor(props) {
    super(props)

    this.state = {
      timeLeft: priceTimeout,
      timeLeftStr: '3:00'
    }

    this.countdown = this.countdown.bind(this)
  }

  componentDidMount() {
    if (!this.props.orderNumber) {
      this.props.history.push('/')
    }

    if (this.props.apiSet) {
      this.props.getOrderPrice({
        orderNumber: this.props.orderNumber
      })

      this.countdown()
    }

    this.setState(state => ({
      ...state,
      interval: setInterval(() => {
        this.countdown()
      }, 1000)
    }))
  }

  componentDidUpdate(prevProps) {
    if (this.props.apiSet && this.props.apiSet !== prevProps.apiSet) {
      this.props.getOrderPrice({
        orderNumber: this.props.orderNumber
      })
    }

    if (this.props.status === constants.receivingPayment) {
      this.props.history.push(`/order/${this.props.orderNumber}`)
    }
  }

  componentWillUnmount() {
    clearInterval(this.state.interval)
  }

  countdown() {
    const date = this.props.date ? new Date(this.props.date) : new Date()
    const timeLeft = priceTimeout - Date.now() + date.getTime()
    const orderNumber = this.props.orderNumber

    if (timeLeft < 0) {
      return this.props.getOrderPrice({
        orderNumber
      })
    }

    this.setState({ timeLeft, timeLeftStr: timeToStr(timeLeft) })
  }

  render() {
    const price = this.props.price

    return (
      <section className="section">
        <div className="container">
          <div className="box bc-box">
            <div className="columns">
              <div className="column">
                <Form />
              </div>

              <div className="column has-text-right-tablet bc-right-column">
                <h1 className="title is-4">Стоимость BTC</h1>
                <h2 className="subtitle is-5">
                  <NumberFormat value={price} thousandSeparator={' '} decimalSeparator={','} decimalScale={2} fixedDecimalScale={true} displayType={'text'} suffix={' тг.'} />
                </h2>

                <p>
                  <span>{this.state.timeLeftStr}</span>
                  <span className={`image is-24x24 bc-clock`}>
                    <img src={clockIcon} alt="clock" />
                  </span>
                  <br /><br />
                  Курс зафиксирован на 3 минуты. По истечении этого времени цена может обновиться.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

NewOrder.propTypes = {
  price: PropTypes.number,
  orderNumber: PropTypes.number,
  date: PropTypes.string,
  getOrderPrice: PropTypes.func,
  history: PropTypes.object,
  status: PropTypes.string,
  apiSet: PropTypes.bool
}

const mapStateToProps = state => ({
  price: state.order.price,
  orderNumber: state.order.orderNumber,
  date: state.order.date,
  apiSet: state.session.apiSet,
  status: state.order.status
})

export default withRouter(connect(mapStateToProps, {
  getOrderPrice: orderActions.getPrice
})(NewOrder))
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Input from '../../components/Input'
import kaspiImg from '../../assets/images/kaspi.png'
import bitcoinImg from '../../assets/images/bitcoin.png'
import cashImg from '../../assets/images/cash.png'
import { orderActions } from '../../redux/order'
import { validateKaspi, validateBTCAddress } from '../../helpers/validations'

export class DynamicFields extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      btcFocused: false,
      kztFocused: false,
      kaspiError: null,
      isKaspiValid: false,
      isBTCAddressValid: false,
      isKaspiCardValid: false
    }

    this.onBTCBlur = this.onBTCBlur.bind(this)
    this.onBTCFocus = this.onBTCFocus.bind(this)
    this.onKZTBlur = this.onKZTBlur.bind(this)
    this.onKZTFocus = this.onKZTFocus.bind(this)
    this.updateBTC = this.updateBTC.bind(this)
    this.updateKZT = this.updateKZT.bind(this)
    this.updateBTCAddress = this.updateBTCAddress.bind(this)
    this.updateKaspiCard = this.updateKaspiCard.bind(this)
  }

  onKZTFocus() {
    this.setState(state => ({ ...state, kztFocused: true }))
  }

  onKZTBlur() {
    this.setState(state => ({ ...state, kztFocused: false }))
  }

  onBTCFocus() {
    this.setState(state => ({ ...state, btcFocused: true }))
  }

  onBTCBlur() {
    this.setState(state => ({ ...state, btcFocused: false }))
  }

  updateBTC(values) {
    if (this.state.btcFocused) {
      this.props.updateBTC(values.floatValue)
    }
  }

  updateKZT(values) {
    if (this.state.kztFocused) {
      this.props.updateKZT(values.floatValue)
    }
  }

  updateBTCAddress(event) {
    event.preventDefault()
    this.props.updateBTCAddress(event.target.value)
  }

  updateKaspiCard(event) {
    event.preventDefault()
    this.props.updateKaspiCard(event.target.value)
  }

  validateKaspi() {
    const kaspiError = validateKaspi(this.props.amountKZT)

    const isKaspiValid = kaspiError ? false : true

    if (isKaspiValid !== this.state.isKaspiValid) {
      this.setState(state => ({ ...state, isKaspiValid, kaspiError }))
    }
  }

  validateBTCAddress() {
    const btcAddressError = validateBTCAddress(this.props.btcAddress)

    const isBTCAddressValid = btcAddressError ? false : true

    this.setState(state => ({ ...state, isBTCAddressValid, btcAddressError }))
  }

  validateKaspiCard() {
    const kaspiRE = /^516949\d{10}$/

    const kaspiCardError = kaspiRE.test(this.props.kaspiCard) ? null : 'Некорректный формат номера карты'

    const isKaspiCardValid = kaspiCardError ? false : true

    this.setState(state => ({ ...state, isKaspiCardValid, kaspiCardError }))
  }

  componentDidMount() {
    if (this.props.type === 'kaspibtc' || this.props.type === 'btckaspi') {
      this.validateKaspi()

      if (this.props.btcAddress) {
        this.validateBTCAddress()
      }

      if (this.props.kaspiCard) {
        this.validateKaspiCard()
      }

      this.props.setFormValid(this.state.isKaspiValid && this.state.isBTCAddressValid && this.state.isKaspiCardValid)
    }

    this.props.setFormValid(true)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.price !== prevProps.price) {
      this.state.btcFocused ? this.props.updateBTC(this.props.amountBTC) : this.props.updateKZT(this.props.amountKZT)
    }

    if (this.props.amountKZT !== prevProps.amountKZT) {
      switch (this.props.type) {
        case 'kaspibtc':
          this.validateKaspi()
          break
        case 'btckaspi':
          this.validateKaspi()
          break
        default:
          break
      }
    }

    if (this.props.btcAddress !== prevProps.btcAddress) {
      this.validateBTCAddress()
    }

    if (this.props.kaspiCard !== prevProps.kaspiCard) {
      this.validateKaspiCard()
    }

    const validationChanged = this.state.isKaspiValid !== prevState.isKaspiValid || this.state.isBTCAddressValid !== prevState.isBTCAddressValid || this.state.isKaspiCardValid !== prevState.isKaspiCardValid

    if (validationChanged) {
      switch (this.props.type) {
        case 'kaspibtc':
          this.props.setFormValid(this.state.isKaspiValid && this.state.isBTCAddressValid)
          break
        case 'btckaspi':
          this.props.setFormValid(this.state.isKaspiValid && this.state.isKaspiCardValid)
          break
        default:
          break
      }
    }
  }

  render() {
    const { btcAddress, amountBTC, amountKZT, type, kaspiCard } = this.props

    switch (type) {
      case 'kaspibtc':
        return <div>
          <Input
            type="number"
            img={kaspiImg}
            label={`Отправляете`}
            value={amountKZT}
            decimals={0}
            suffix=" тг."
            onFocus={this.onKZTFocus}
            onBlur={this.onKZTBlur}
            onChange={this.updateKZT}
            error={this.state.kaspiError}
          />

          <Input
            type="number"
            img={bitcoinImg}
            label={`Получаете`}
            value={amountBTC}
            decimals={5}
            suffix=" BTC"
            onFocus={this.onBTCFocus}
            onBlur={this.onBTCBlur}
            onChange={this.updateBTC}
          />

          <Input
            type="text"
            label={`Адрес BTC-кошелька`}
            value={btcAddress}
            onChange={this.updateBTCAddress}
            error={this.state.btcAddressError}
          />
        </div>

      case 'btckaspi':
        return <div>
          <Input
            type="number"
            img={bitcoinImg}
            label={`Отправляете`}
            value={amountBTC}
            decimals={5}
            suffix=" BTC"
            onFocus={this.onBTCFocus}
            onBlur={this.onBTCBlur}
            onChange={this.updateBTC}
          />

          <Input
            type="number"
            img={kaspiImg}
            label={`Получаете`}
            value={amountKZT}
            decimals={0}
            suffix=" тг."
            onFocus={this.onKZTFocus}
            onBlur={this.onKZTBlur}
            onChange={this.updateKZT}
            error={this.state.kaspiError}
          />

          <Input
            type="text"
            label={`Номер карты Kaspi`}
            value={kaspiCard}
            onChange={this.updateKaspiCard}
            error={this.state.kaspiCardError}
          />
        </div>

      case 'cashbtc':
        return <div>
          <Input
            type="number"
            img={cashImg}
            label={`Отдаете`}
            value={amountKZT}
            decimals={0}
            suffix=" тг."
            onFocus={this.onKZTFocus}
            onBlur={this.onKZTBlur}
            onChange={this.updateKZT}
          />

          <Input
            type="number"
            img={bitcoinImg}
            label={`Получаете`}
            value={amountBTC}
            decimals={5}
            suffix=" BTC"
            onFocus={this.onBTCFocus}
            onBlur={this.onBTCBlur}
            onChange={this.updateBTC}
          />
        </div>

      case 'btccash':
        return <div>
          <Input
            type="number"
            img={bitcoinImg}
            label={`Отправляете`}
            value={amountBTC}
            decimals={5}
            suffix=" BTC"
            onFocus={this.onBTCFocus}
            onBlur={this.onBTCBlur}
            onChange={this.updateBTC}
          />

          <Input
            type="number"
            img={cashImg}
            label={`Получаете`}
            value={amountKZT}
            decimals={0}
            suffix=" тг."
            onFocus={this.onKZTFocus}
            onBlur={this.onKZTBlur}
            onChange={this.updateKZT}
          />
        </div>

      default:
        return null
    }
  }
}

DynamicFields.propTypes = {
  amountKZT: PropTypes.number,
  amountBTC: PropTypes.number,
  btcAddress: PropTypes.string,
  type: PropTypes.string,
  updateBTC: PropTypes.func,
  updateKZT: PropTypes.func,
  updateBTCAddress: PropTypes.func,
  price: PropTypes.number,
  kaspiCard: PropTypes.string,
  updateKaspiCard: PropTypes.func,
  setFormValid: PropTypes.func
}

const mapStateToProps = state => ({
  price: state.order.price,
  amountKZT: state.order.amountKZT,
  amountBTC: state.order.amountBTC,
  type: state.order.type,
  btcAddress: state.order.btcAddress,
  kaspiCard: state.order.kaspiCard
})

export default connect(mapStateToProps, {
  updateBTC: orderActions.updateBTC,
  updateKZT: orderActions.updateKZT,
  updateBTCAddress: orderActions.updateBTCAddress,
  updateKaspiCard: orderActions.updateKaspiCard,
  setFormValid: orderActions.setFormValid
})(DynamicFields)
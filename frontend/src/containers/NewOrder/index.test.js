/* eslint-env jest */

import React from 'react'
import { NewOrder } from './index'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

test('renders without crashing', () => {
  const history = {
    push: jest.fn()
  }

  const component = shallow(
    <NewOrder history={history} />
  )
  expect(component.debug()).toMatchSnapshot()
})

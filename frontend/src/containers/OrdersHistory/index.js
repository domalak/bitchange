import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter, Link } from 'react-router-dom'
import styles from './index.module.scss'
import { getOrdersHistory } from '../../redux/orders/actions'

import { orderStatus, orderType, orderSumm } from './functions'

export class OrdersHistory extends Component {
  constructor() {
    super()
    this.state = {
      OrdersHistory: []
    }
  }

  componentDidMount() {
    if (this.props.apiSet) {
      this.props.getOrdersHistory()
      this.setState({ OrdersHistory: this.props.orders})
    }
  }
  
  componentDidUpdate(prevProps) {
    if (this.props.apiSet && this.props.apiSet !== prevProps.apiSet) {
      this.props.getOrdersHistory()
    }

    if (this.props.orders && this.props.orders !== prevProps.orders) {
      this.setState({ OrdersHistory: this.props.orders})
    }
  }

  render() {
    const orders = this.state.OrdersHistory.map((item) => (
      <Link to={`/order/${item.orderNumber}`} key={item.id}>
        <div className={`${styles.orderCart} ${styles.item}`}>
          <div className={`${styles.orderNumber} ${styles.orderItem}`}>{item.orderNumber}</div>
          <div className={`${styles.orderItem} ${styles.orderType}`}>{orderType(item.type)}</div>
          <div className={`${styles.orderSumm} ${styles.orderItem}`}>{orderSumm(item.type, item.amountKZT, item.amountBTC)}</div>
          <div className={`${styles.orderItem} ${styles.orderStatus}`}>{orderStatus(item.status)}</div>
        </div>
      </Link>
    ))

    const noOrders = (<p> Пока заказов нет...</p>)
    
    return (
      <section className="section">
        <div className="container">
          <div className="box bc-box">
            <div className="columns">
              <div className="column">
                <h1 className={`title is-4`}>История Ваших Заказов</h1>
                <div className={styles.orderCart}>
                  <div className={`${styles.orderNumber} ${styles.orderItem} ${styles.title}`}>№ Заказа</div>
                  <div className={`${styles.orderType} ${styles.orderItem} ${styles.title}`}>Тип заказа</div>
                  <div className={`${styles.orderSumm} ${styles.orderItem} ${styles.title}`}>Сумма транзакции</div>
                  <div className={`${styles.orderStatus} ${styles.orderItem} ${styles.title}`}>Cтатус</div>
                </div>
                {this.state.OrdersHistory.length > 0 ? orders : noOrders}
                
              </div>

            </div>

          </div>

        </div>

      </section>
    )
  }
}

OrdersHistory.propTypes = {
  orders: PropTypes.array,
  getOrdersHistory: PropTypes.func,
  apiSet: PropTypes.bool
}

const mapStateToProps = state => ({
  orders: state.orders.orders,
  apiSet: state.session.apiSet
})

export default withRouter(connect(mapStateToProps, {
  getOrdersHistory
})(OrdersHistory))
import React from 'react'
//images:
import kaspiImg from '../../assets/images/kaspi.png'
import bitcoinImg from '../../assets/images/bitcoin.png'
import arrowRightImg from '../../assets/icons/arrow-right.svg'
import cashImg from '../../assets/images/cash.png'

import styles from './index.module.scss'
import constants from '../../constants'

export const orderStatus = (item) => {
    switch(item){
        case constants.receivingPayment:
          return <p className={styles.status}>Проверяется</p>
        case constants.userSentPayment:
          return <p className={styles.status}>Пользователь произвел оплату</p>
        case constants.sendingPayment:
          return <p className={styles.status}>Перевод пользователю произведен</p>
        case constants.complete:
          return <p className={styles.statusGreen}>Одобрено</p>
        case constants.canceled:
          return <p className={styles.statusGray}>Отклонено</p>
        default:
          return <p className={styles.statusGray}>Пользователь не зарегистрирован</p>
    }
}

export const orderType = (item) => {
    switch(item){
        case 'kaspibtc':
          return <p>
            <span className={`image is-24x24 ${styles.inline}`}><img src={kaspiImg} className={styles.icon} alt="kaspi" /></span>
            Kaspi
            <span className={`image is-24x24 ${styles.inline}`}><img src={arrowRightImg} className={`${styles.arrow}`} alt="icon" /></span>
            <span className={`image is-24x24 ${styles.inline}`}><img src={bitcoinImg} className={styles.icon} alt="icon" /></span>
            Биткоины
          </p>
        case 'btckaspi':
          return <p>
            <span className={`image is-24x24 ${styles.inline}`}><img src={bitcoinImg} className={styles.icon} alt="icon" /></span> Биткоины
            <span className={`image is-24x24 ${styles.inline}`}><img src={arrowRightImg} className={`${styles.arrow}`} alt="icon" /></span>
            <span className={`image is-24x24 ${styles.inline}`}><img src={kaspiImg} className={styles.icon} alt="kaspi" /></span> Kaspi
          </p>
        case 'cashbtc':
          return <p>
            <span className={`image is-24x24 ${styles.inline}`}><img src={cashImg} className={styles.icon} alt="icon" /></span> Наличные
            <span className={`image is-24x24 ${styles.inline}`}><img src={arrowRightImg} className={`${styles.arrow}`} alt="icon" /></span>
            <span className={`image is-24x24 ${styles.inline}`}><img src={bitcoinImg} className={styles.icon} alt="icon" /></span> Биткоины
          </p>
        case 'btccash':
          return <p>
            <span className={`image is-24x24 ${styles.inline}`}><img src={bitcoinImg} className={styles.icon} alt="icon" /></span> Биткоины
            <span className={`image is-24x24 ${styles.inline}`}><img src={arrowRightImg} className={`${styles.arrow}`} alt="icon" /></span>
            <span className={`image is-24x24 ${styles.inline}`}><img src={cashImg} className={styles.icon} alt="icon" /></span> Наличные
          </p>
        default:
          return <p> </p>
      }
}

export const orderSumm = (type, amountKZT, amountBTC) => {
    switch(type){
        case 'kaspibtc':
          return <p>
            <span className={`image is-24x24 ${styles.inline}`}><img src={kaspiImg} className={styles.icon} alt="kaspi" /></span>{amountKZT.toFixed(0)} тг.
            <span className={`image is-24x24 ${styles.inline}`}><img src={arrowRightImg} className={`${styles.arrow}`} alt="icon" /></span>
            <span className={`image is-24x24 ${styles.inline}`}><img src={bitcoinImg} className={styles.icon} alt="icon" /></span>{amountBTC.toFixed(5)} BTC
          </p>
        case 'btckaspi':
          return <p>
            <span className={`image is-24x24 ${styles.inline}`}><img src={bitcoinImg} className={styles.icon} alt="icon" /></span> {amountBTC.toFixed(5)} BTC
            <span className={`image is-24x24 ${styles.inline}`}><img src={arrowRightImg} className={`${styles.arrow}`} alt="icon" /></span>
            <span className={`image is-24x24 ${styles.inline}`}><img src={kaspiImg} className={styles.icon} alt="kaspi" /></span> {amountKZT.toFixed(0)} тг.
          </p>
        case 'cashbtc':
          return <p>
            <span className={`image is-24x24 ${styles.inline}`}><img src={cashImg} className={styles.icon} alt="icon" /></span> {amountKZT.toFixed(0)} тг.
            <span className={`image is-24x24 ${styles.inline}`}><img src={arrowRightImg} className={`${styles.arrow}`} alt="icon" /></span>
            <span className={`image is-24x24 ${styles.inline}`}><img src={bitcoinImg} className={styles.icon} alt="icon" /></span> {amountBTC.toFixed(5)} BTC
          </p>
        case 'btccash':
          return <p>
            <span className={`image is-24x24 ${styles.inline}`}><img src={bitcoinImg} className={styles.icon} alt="icon" /></span> {amountBTC.toFixed(5)} BTC
            <span className={`image is-24x24 ${styles.inline}`}><img src={arrowRightImg} className={`${styles.arrow}`} alt="icon" /></span>
            <span className={`image is-24x24 ${styles.inline}`}><img src={cashImg} className={styles.icon} alt="icon" /></span> {amountKZT.toFixed(0)} тг.
          </p>
        default:
          return <p> </p>
      }
}


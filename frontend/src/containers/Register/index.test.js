/* eslint-env jest */

import React from 'react'
import { Register } from './index'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

test('renders without crashing', () => {
  const component = shallow(
    <Register />
  )
  expect(component.debug()).toMatchSnapshot()
})

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { registerUserAction, logout } from '../../redux/session/actions'
import { withRouter, Link } from 'react-router-dom'
import Input from '../../components/Input'
import FormButton from '../../components/FormButton'
import styles from './index.module.scss'
import { validateRegisterInputPhone } from './Validate'
import { validateRegisterInputPassword } from './Validate'

export class Register extends Component {
  constructor() {
    super()
    this.state = {
      isChecked: false,
      phone: '',
      password: '',
      phoneError: '',
      passwordError: ' ',
      isValid: false
    }
    this.onHandleRegistration = this.onHandleRegistration.bind(this)
    this.onHandleTerms = this.onHandleTerms.bind(this)
    this.onPhoneChange = this.onPhoneChange.bind(this)
    this.onPasswordChange = this.onPasswordChange.bind(this)
    this.isValidCheck = this.isValidCheck.bind(this)
  }

  componentDidMount() {
    if (this.props.token) {
      this.props.history.push('/verification')
    }
  }

  onHandleRegistration(event) {
    this.setState({ errors: {} })
    event.preventDefault()

    let phone = this.state.phone
    let password = this.state.password

    const data = {
      phone, password
    }
    
    this.props.registerUserAction(data)
  }

  isValidCheck() {
    if(this.state.isChecked && !this.state.phoneError && !this.state.passwordError){
      return this.setState({ ...this.state, isValid: true })
    }

    this.setState({ ...this.state, isValid: false })
  }

  onHandleTerms(event) {
    let acceptedTerm = event.target.checked
    this.setState({ ...this.state, isChecked: acceptedTerm })
  }

  onPhoneChange(event) {
    this.setState({ phone: event.target.value })
    let { phoneError } = validateRegisterInputPhone( event.target.value )
    this.setState({ phoneError })
    if(phoneError){
      this.setState({ isValid: false })
    }
  }

  onPasswordChange(event) {
    this.setState({ password: event.target.value })
    let { passwordError } = validateRegisterInputPassword( event.target.value )
    this.setState({ passwordError })  
    if(passwordError){
      this.setState({ isValid: false })
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.token) {
      this.props.history.push('/verification')
    }
    if (this.props.errors && this.props.errors.password && (this.props.errors.password !== prevProps.errors.password || this.props.errors.phone !== prevProps.errors.phone)) {
      this.setState({ passwordError: this.props.errors.password, phoneError: this.props.errors.phone })
    }

    if (this.state.phone !== prevState.phone || this.state.password !== prevState.password || this.state.isChecked !== prevState.isChecked) {
      this.isValidCheck()
    }

    if(this.props.errors && this.props.errors.phone && this.props.errors !== prevProps.errors){
      this.setState({phoneError: this.props.errors.phone})
    }
  }

  render() {
    return (
      <section className="section">
        <div className="container">
          <div className="box bc-box">
            <div className="columns">
              <div className="column">
                <h1 className={`title is-4`}>Регистрация</h1>
                <h2 className={`subtitle is-5 bc-subtitle`}>Пожалуйста заполните внимательно все поля.</h2>
                <form onSubmit={this.onHandleRegistration}>
                  <Input
                      type="phone"
                      label={`Ваш телефон`}
                      value={this.state.phone}
                      onChange={this.onPhoneChange}
                      error={this.state.phoneError}
                    />
                    
                  <Input
                      type="password"
                      label={`Ваш Пароль`}
                      value={this.state.password}
                      onChange={this.onPasswordChange}
                      error={this.state.passwordError}
                    />
                  
                  <div className={`field is-horizontal bc-field`}>
                    <div className="field-label" style={{flexGrow: 2}}></div>
                    <div className="field-body">
                      <div className="field">
                        <div className="control">
                          <label className="checkbox">
                            <input type="checkbox" className={styles.checkbox} name="checkbox" id="checkbox" onChange={this.onHandleTerms}/>
                            Я принимаю условия <a href="/user-agreement">Пользовательского Соглашения</a>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <FormButton disabled={!this.state.isValid} label="Зарегистрироваться"/>
                </form>
              </div>
              <div className="column has-text-right-tablet bc-right-column">
                <h1 className="title is-4">&nbsp;</h1>
                <h2 className="subtitle is-5">Уже зарегистрированы? <Link to="/login">Войти в систему.</Link></h2>
              </div>
            </div>

          </div>

        </div>

      </section>
    )
  }
}

Register.propTypes = {
  registerUserAction: PropTypes.func,
  history: PropTypes.object,
  token: PropTypes.string,
  errors: PropTypes.object,
}

const mapStateToProps = state => ({
  token: state.session.token,
  errors: state.session.errors ? state.session.errors : null
})

export default withRouter(connect(mapStateToProps, {
  registerUserAction, logout
})(Register))
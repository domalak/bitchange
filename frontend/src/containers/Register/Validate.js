export const validateRegisterInputPhone = (phone) => {
  let phoneError = ''
  // При отсутствии введенных параметров переводим их в пустую строку для проверки через валидатор
  phone = (phone !== undefined) ? phone : ""

  // Phone checks
  if (phone === "") {
    phoneError = "Телефонный номер не введен"
  }

  //проверка на принадлежность введенного телефона к Казастанским операторам
  let regEx = /^((7)([0-9]){9})$/
  if (!regEx.test(phone)) {
    phoneError = "Телефонный номер введен неправильно"
  }

  return {
    phoneError
  }
}

export const validateRegisterInputPassword = (password) => {
  let passwordError = ''
  // При отсутствии введенных параметров переводим их в пустую строку для проверки через валидатор
  password = (password !== undefined) ? password : ""

  // Проверяем введен ли пароль
  if (password === "") {
    passwordError = "Пароль не введен"
  }

  // Задаем паролю минимальную длину в 8 символов
  if (password.length < 8) {
    passwordError = "Длина пароля должна состовлять от 8 символов"
  }

  return {
    passwordError
  }
}
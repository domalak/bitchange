import React, { Component } from 'react'
import NumberFormat from 'react-number-format'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { orderActions } from '../../redux/order'
import Form from './Form'
import constants from '../../constants'
import clockIcon from '../../assets/icons/clock.svg'

const priceTimeout = 15 * 60 * 1000

function timeToStr(time) {
  const minutes = time > 0 ? Math.floor(time / 60000) : 0
  const seconds = Math.abs(Math.floor((time % 60000) / 1000))
  const secondsStr = seconds < 10 ? '0' + seconds : seconds

  return minutes + ':' + secondsStr
}


export class AdminOrder extends Component {
  componentDidMount() {
    if (!this.props.match.params.id) {
      this.props.history.push('/admin/orders')
    }

    if (this.props.apiSet) {
      this.props.getOrder({
        orderNumber: this.props.match.params.id
      })
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.apiSet !== prevProps.apiSet && this.props.apiSet) {
      this.props.getOrder({
        orderNumber: this.props.match.params.id
      })
    }
  }

  dynamicInstructions() {
    if (this.props.status === constants.receivingPayment) {
      if (this.props.type === 'kaspibtc') {
        return <div>
          <p>
            <span>{this.state.timeLeftStr}</span>
            <span className="image is-24x24 bc-clock">
              <img src={clockIcon} alt="clock" />
            </span>
            <br /><br />
            Просим Вас сделать перевод на карту Kaspi, указанную ниже, в течение 15 минут. По истечении этого времени цена может обновиться.
            <br /><br />
            Карта Kaspi:
        </p>
          <div className="notification">
            5169 4971 0139 0270
        </div>
        </div>
      }
      if (this.props.type === 'btckaspi') {
        return <div>
          <p>
            <span style={{ marginRight: 10 + 'px' }}>{this.state.timeLeftStr}</span>
            <span className="image is-24x24 bc-clock">
              <img src={clockIcon} alt="clock" />
            </span>
            <br /><br />
            Просим Вас сделать перевод на адрес кошелька Bitcoin, указанный ниже, в течение 15 минут. По истечении этого времени цена может обновиться.
            <br /><br />
            Адрес Bitcoin:
        </p>
        <div className="notification">
          bc1qdlp4sz3rpvwaf38k994umpvr3plyp7qmac6pjx
        </div>
        </div>
      }
    }

    return null
  }

  render() {
    const price = this.props.price

    return (
      <section className="section">
        <div className="container">
          <div className="box bc-box">
            <div className="columns">
              <div className="column">
                <Form />
              </div>

              <div className="column has-text-right">
                <h1 className="title is-4">Стоимость BTC</h1>
                <h2 className="subtitle is-5">
                  <NumberFormat value={price} thousandSeparator={' '} decimalSeparator={','} decimalScale={2} fixedDecimalScale={true} displayType={'text'} suffix={' тг.'} />
                </h2>

                {this.dynamicInstructions()}
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

AdminOrder.propTypes = {
  price: PropTypes.number,
  orderNumber: PropTypes.number,
  amountKZT: PropTypes.number,
  amountBTC: PropTypes.number,
  type: PropTypes.string,
  date: PropTypes.string,
  getOrder: PropTypes.func,
  history: PropTypes.object,
  token: PropTypes.string,
  status: PropTypes.string,
  match: PropTypes.object,
  apiSet: PropTypes.bool
}

const mapStateToProps = state => ({
  price: state.order.price,
  amountKZT: state.order.amountKZT,
  amountBTC: state.order.amountBTC,
  orderNumber: state.order.orderNumber,
  type: state.order.type,
  date: state.order.date,
  token: state.session.token,
  status: state.order.status,
  apiSet: state.session.apiSet
})

export default withRouter(connect(mapStateToProps, {
  getOrder: orderActions.get
})(AdminOrder))
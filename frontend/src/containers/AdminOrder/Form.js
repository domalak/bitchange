import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { orderActions } from '../../redux/order'
import DynamicFields from './DynamicFields'

export class Form extends Component {
  constructor(props) {
    super(props)

    this.updateOrder = this.updateOrder.bind(this)
  }

  updateOrder() {
  }

  render() {
    return (
      <div>
        <h1 className={`title is-4`}>Заказ админка</h1>
        <h2 className={`subtitle is-5 bc-subtitle`}>Пожалуйста заполните внимательно все поля.</h2>

        <DynamicFields />

      </div>
    )
  }
}

Form.propTypes = {
  updateOrder: PropTypes.func
}

const mapStateToProps = state => ({
  order: state.order
})

export default connect(mapStateToProps, {
  updateOrder: orderActions.update
})(Form)
/* eslint-env jest */

import React from 'react'
import { DynamicFields } from './DynamicFields'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

test('renders without crashing', () => {
  const match = {
    params: {
      id: 1
    }
  }

  const component = shallow(
    <DynamicFields match={match} setFormValid={jest.fn} />
  )
  expect(component.debug()).toMatchSnapshot()
})

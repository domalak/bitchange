import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Input from '../../components/Input'
import kaspiImg from '../../assets/images/kaspi.png'
import bitcoinImg from '../../assets/images/bitcoin.png'
import cashImg from '../../assets/images/cash.png'
import { orderActions } from '../../redux/order'

export class DynamicFields extends React.Component {
  constructor(props) {
    super(props)

    this.updateKZT = this.updateKZT.bind(this)
  }

  updateKZT(values) {
    this.props.updateKZT(values.floatValue)
  }

  componentDidUpdate(prevProps) {
    if (this.props.price !== prevProps.price) {
      this.props.updateKZT(this.props.amountKZT)
    }
  }

  render() {
    const { btcAddress, amountBTC, amountKZT, type, kaspiCard } = this.props

    switch (type) {
      case 'kaspibtc':
        return <div>
          <Input
            type="number"
            img={kaspiImg}
            label={`Отправляете`}
            value={amountKZT}
            decimals={0}
            suffix=" тг."
            disabled={true}
          />

          <Input
            type="number"
            img={bitcoinImg}
            label={`Получаете`}
            value={amountBTC}
            decimals={5}
            suffix=" BTC"
            disabled={true}
          />

          <Input
            type="text"
            label={`Адрес BTC-кошелька`}
            value={btcAddress}
            disabled={true}
          />
        </div>

      case 'btckaspi':
        return <div>
          <Input
            type="number"
            img={bitcoinImg}
            label={`Отправляете`}
            value={amountBTC}
            decimals={5}
            suffix=" BTC"
            disabled={true}
          />

          <Input
            type="number"
            img={kaspiImg}
            label={`Получаете`}
            value={amountKZT}
            decimals={0}
            suffix=" тг."
            disabled={true}
          />

          <Input type="text" label={`Номер карты Kaspi Gold`} value={kaspiCard} disabled={true} />
        </div>

      case 'cashbtc':
        return <div>
          <Input
            type="number"
            img={cashImg}
            label={`Отдаете`}
            value={amountKZT}
            decimals={0}
            suffix=" тг."
            disabled={true}
          />

          <Input
            type="number"
            img={bitcoinImg}
            label={`Получаете`}
            value={amountBTC}
            decimals={5}
            suffix=" BTC"
            disabled={true}
          />
        </div>

      case 'btccash':
        return <div>
          <Input
            type="number"
            img={bitcoinImg}
            label={`Отправляете`}
            value={amountBTC}
            decimals={5}
            suffix=" BTC"
            disabled={true}
          />

          <Input
            type="number"
            img={cashImg}
            label={`Получаете`}
            value={amountKZT}
            decimals={0}
            suffix=" тг."
            disabled={true}
          />
        </div>

      default:
        return null
    }
  }
}

DynamicFields.propTypes = {
  amountKZT: PropTypes.number,
  amountBTC: PropTypes.number,
  btcAddress: PropTypes.string,
  type: PropTypes.string,
  updateKZT: PropTypes.func,
  price: PropTypes.number,
  kaspiCard: PropTypes.string
}

const mapStateToProps = state => ({
  price: state.order.price,
  amountKZT: state.order.amountKZT,
  amountBTC: state.order.amountBTC,
  type: state.order.type,
  btcAddress: state.order.btcAddress,
  kaspiCard: state.order.kaspiCard
})

export default connect(mapStateToProps, {
  updateKZT: orderActions.updateKZT
})(DynamicFields)
/* eslint-env jest */

import React from 'react'
import { OutOfHours } from './index'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

test('renders without crashing', () => {
  const component = shallow(
    <OutOfHours />
  )
  
  expect(component.debug()).toMatchSnapshot()
})

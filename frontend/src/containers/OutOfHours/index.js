import React, { Component } from 'react'

export class OutOfHours extends Component {
  render() {
    return (
        <section className="hero is-medium is-primary is-bold">
            <div className="hero-body">
            <div className="container">
                <h1 className="title">
                Страница не доступна вне рабочего времени (10:00-22:00).
                </h1>
                <h2 className="subtitle">
                Пожалуйста, загляните попозже.
                </h2>
            </div>
            </div>
        </section>
    )
  }
}

export default OutOfHours
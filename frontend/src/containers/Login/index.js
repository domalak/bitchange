import React, { Component } from 'react'
// import styles from './index.module.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { loginUserAction } from '../../redux/session/actions'
import { Link, withRouter } from 'react-router-dom'
import Input from '../../components/Input'
import FormButton from '../../components/FormButton'
import { validateRegisterInputPhone } from '../Register/Validate'

export class Login extends Component {
  constructor() {
    super()
    this.state = {
      phone: '',
      password: '',
      phoneNotFound: '',
      passwordIncorrect: '',
      phoneError: '',
      passwordError: ' ',
      isValid: false
    }

    this.onHandleLogin = this.onHandleLogin.bind(this)
    this.onPhoneChange = this.onPhoneChange.bind(this)
    this.onPasswordChange = this.onPasswordChange.bind(this)
  }
  onHandleLogin(event) {
    event.preventDefault()

    let phone = this.state.phone
    let password = this.state.password

    const data = {
      phone, password
    }

    this.props.loginUserAction(data)
  }

  onPhoneChange(event) {
    this.setState({ phone: event.target.value, phoneNotFound: '' })
    let { phoneError } = validateRegisterInputPhone( event.target.value )
    this.setState({ phoneError })
    if(phoneError){
      this.setState({ isValid: false })
    }
  }

  onPasswordChange(event) {
    this.setState({ password: event.target.value })
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.token && this.props.token !== prevProps.token) {
      const returnUrl = this.props.location.state ? this.props.location.state.returnUrl : '/'
      this.props.history.push(returnUrl)
    }

    if (this.props.errors && prevProps.errors && (this.props.errors.password !== prevProps.errors.password || this.props.errors.phone !== prevProps.errors.phone)) {
      this.setState({ passwordError: this.props.errors.password, phoneError: this.props.errors.phone })
    }

    if(!this.state.phoneError && (this.state.phoneError !== prevState.phoneError)){
      this.setState({isValid: true })
    }
    if(this.props.errors && this.props.errors.phoneNotFound && this.props.errors !== prevProps.errors){
      this.setState({phoneNotFound: this.props.errors.phoneNotFound})
    }
    if(this.props.errors && this.props.errors.passwordIncorrect && this.props.errors !== prevProps.errors){
      this.setState({ passwordIncorrect: this.props.errors.passwordIncorrect})
    }
  }


  componentDidMount() {
    if (this.props.token) {
      this.props.history.push('/')
    }
  }

  render() {
    return (
      <section className="section">
        <div className="container">
          <div className="box bc-box">
            <div className="columns">
              <div className="column">
                <h1 className={`title is-4`}>Вход в систему</h1>
                <h2 className={`subtitle is-5 bc-subtitle`}>Пожалуйста заполните внимательно все поля.</h2>
                <form onSubmit={this.onHandleLogin}>
                  <Input
                    type="phone"
                    label={`Ваш телефон`}
                    value={this.state.phone}
                    onChange={this.onPhoneChange}
                    error={this.state.phoneNotFound || this.state.phoneError}
                  />

                  <Input
                    type="password"
                    label={`Ваш Пароль`}
                    value={this.state.password}
                    onChange={this.onPasswordChange}
                    error={this.state.passwordIncorrect || this.state.passwordError}
                  />

                  <FormButton disabled={!this.state.isValid} label="Войти" />
                </form>
              </div>
              <div className="column has-text-right-tablet bc-right-column">
                <h1 className="title is-4">&nbsp;</h1>
                <h2 className="subtitle is-5">У вас еще нет учетной записи? <Link to="/register">Зарегистрируйтесь!</Link></h2>
              </div>
            </div>

          </div>

        </div>

      </section>
    )
  }
}

Login.propTypes = {
  loginUserAction: PropTypes.func,
  history: PropTypes.object,
  token: PropTypes.string,
  errors: PropTypes.object,
  location: PropTypes.object
}

const mapStateToProps = state => ({
  token: state.session.token,
  errors: state.session.errors,
})

export default withRouter(connect(mapStateToProps, {
  loginUserAction
})(Login))
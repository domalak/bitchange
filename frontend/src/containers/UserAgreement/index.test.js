/* eslint-env jest */

import React from 'react'
import UserAgreement from './index'
import renderer from 'react-test-renderer'

it('renders without crashing', () => {
  const component = renderer.create(
    <UserAgreement />
  )
  expect(component.toJSON()).toMatchSnapshot()
})

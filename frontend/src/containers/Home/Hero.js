import React, { Component } from 'react'
import kaspiImg from '../../assets/images/kaspi.png'
import bitcoinImg from '../../assets/images/bitcoin.png'
import arrowRightImg from '../../assets/icons/arrow-right.svg'
import cashImg from '../../assets/images/cash.png'
import styles from './index.module.scss'
import { Link, withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'

export class Hero extends Component {
  constructor(props) {
    super(props)

    this.heroFootRef = React.createRef()
  }
  
  componentDidMount() {
    window.twttr = (function (d, s, id) {
      let js
      const fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {}
      if (d.getElementById(id)) return t
      js = d.createElement(s)
      js.id = id
      js.src = "https://platform.twitter.com/widgets.js"
      fjs.parentNode.insertBefore(js, fjs)

      t._e = []
      t.ready = function (f) {
        t._e.push(f)
      }

      return t
    }(document, "script", "twitter-wjs"))

    document.getElementById('twitter-wjs').onload = () => window.twttr.widgets.load(
      document.getElementById("twitter-container")
    )

    if (window.twttr.widgets) {
      window.twttr.widgets.load(
        document.getElementById("twitter-container")
      )
    }
  }

  componentDidUpdate() {
    // this.heroFootRef.current.scrollIntoView()
  }

  render() {
    const { location } = this.props

    return (
      <section className="hero is-dark">
        <div className={`hero-body ${styles.heroBody}`}>
          <div className={`container ${styles.container}`}>
            <div className="columns is-vcentered">
              <div className={`column ${styles.leftColumn}`}>
                <h1 className={`title ${styles.title}`}>
                  BitChange - делаем Bitcoin доступным
                  </h1>
              </div>
              <div className={`column bc-right-column ${styles.rightColumn}`}>
                <div className={`has-text-centered ${styles.twitterContainer}`}>
                  <a className="twitter-timeline" href="https://twitter.com/BitChangeKZ?ref_src=twsrc%5Etfw" data-chrome="nofooter" data-height="506">Твиттер Bitchange</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="hero-foot" ref={this.heroFootRef}>
          <nav className="tabs is-boxed is-fullwidth">
            <div className="container">
              <ul>
                <li className={location.pathname === '/home/kaspibtc' ? 'is-active' : ''}>
                  <Link to="/home/kaspibtc">
                    <img src={kaspiImg} className={styles.icon} alt="kaspi" />
                    <span className="is-hidden-mobile">Kaspi</span>
                    <img src={arrowRightImg} className={`${styles.arrow}`} alt="icon" />
                    <img src={bitcoinImg} className={styles.icon} alt="icon" />
                    <span className="is-hidden-mobile">BTC</span>
                    </Link>
                </li>
                <li className={location.pathname === '/home/btckaspi' ? 'is-active' : ''}>
                  <Link to="/home/btckaspi">
                    <img src={bitcoinImg} className={styles.icon} alt="btc" />
                    <span className="is-hidden-mobile">BTC</span>
                      <img src={arrowRightImg} className={styles.arrow} alt="icon" />
                    <img src={kaspiImg} className={styles.icon} alt="icon" />
                    <span className="is-hidden-mobile">Kaspi</span>
                    </Link>
                </li>
                <li className={location.pathname === '/home/cashbtc' ? 'is-active' : ''}>
                  <Link to="/home/cashbtc">
                    <img src={cashImg} className={styles.icon} alt="icon" />
                    <span className="is-hidden-mobile">Нал</span>
                      <img src={arrowRightImg} className={`${styles.arrow}`} alt="icon" />
                    <img src={bitcoinImg} className={styles.icon} alt="icon" />
                    <span className="is-hidden-mobile">BTC</span>
                    </Link>
                </li>
                <li className={location.pathname === '/home/btccash' ? 'is-active' : ''}>
                  <Link to="/home/btccash">
                    <img src={bitcoinImg} className={styles.icon} alt="icon" />
                    <span className="is-hidden-mobile">BTC</span>
                      <img src={arrowRightImg} className={styles.arrow} alt="icon" />
                    <img src={cashImg} className={styles.icon} alt="icon" />
                    <span className="is-hidden-mobile">Нал</span>
                    </Link>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </section>
    )
  }
}

Hero.propTypes = {
  location: PropTypes.object
}

export default withRouter(Hero)
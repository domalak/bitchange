import React from 'react'
import KaspiBTC from '../KaspiBTC'
import CashBTC from '../CashBTC'
import BTCKaspi from '../BTCKaspi'
import { Route, withRouter } from 'react-router-dom'
import BTCCash from '../BTCCash'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { orderActions } from '../../redux/order'

export class Exchange extends React.Component {
  componentDidMount() {
    if (this.props.orderNumber) {
      this.props.clearOrder()
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.orderNumber && this.props.orderNumber !== prevProps.orderNumber) {
      this.props.history.push('/new-order')
    }
  }

  render() {
    return <section className={`section`} style={{background: 'whitesmoke'}}>
    <div className={`container`}>
      <Route path="/home/kaspibtc" component={KaspiBTC} />
      <Route path="/home/btckaspi" component={BTCKaspi} />
      <Route path="/home/cashbtc" component={CashBTC} />
      <Route path="/home/btccash" component={BTCCash} />
    </div>
  </section>
  }
}

Exchange.propTypes = {
  orderNumber: PropTypes.number,
  history: PropTypes.object,
  clearOrder: PropTypes.func,
}

const mapStateToProps = state => ({
  orderNumber: state.order.orderNumber
})

export default withRouter(connect(mapStateToProps, {
  clearOrder: orderActions.clear
})(Exchange))
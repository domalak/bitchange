import React from 'react'
import Hero from './Hero'
import Exchange from './Exchange'

const Home = () => {
  return <div>
    <Hero />
    <Exchange />
  </div>
}

export default Home
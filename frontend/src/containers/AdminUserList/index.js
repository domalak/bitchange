import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter, Link } from 'react-router-dom'
import styles from './index.module.scss'
import { adminGetUsers } from '../../redux/admin/actions'
import { userStatus } from './functions'

export class AdminUserList extends Component {
  constructor() {
    super()
    this.state = {
      UserList: []
    }
  }

  componentDidMount() {
    if (this.props.apiSet) {
      this.props.adminGetUsers()
    }
    

    if (this.props.users) {
      this.setState({ UserList: this.props.users })
    }
  }
  
  componentDidUpdate(prevProps) {
    if (this.props.apiSet && this.props.apiSet !== prevProps.apiSet) {
      this.props.adminGetUsers()
    }

    if (this.props.users !== prevProps.users) {
      this.setState({ UserList: this.props.users })
    }
  }

  render() {
    const orders = this.state.UserList.map((item) => {
      const createDateFormatted = new Date(item.createDate)
      item.createDateFormatted = createDateFormatted.toLocaleString()


      return <Link to={`/admin/user/${item.phone}`} key={item.id}>
        <div className={`${styles.userListCart} ${styles.userItem}`} key={item.id}>
          <div className={`${styles.userType} ${styles.UserItem}`}>+7 {item.phone}</div>
          <div className={`${styles.userSumm} ${styles.UserItem}`}>{userStatus(item.phoneVerified, item.photoVerifyStatus, item.isAdmin)}</div>
          <div className={`${styles.userSmsCode} ${styles.UserItem}`}>{item.createDateFormatted}</div>
        </div>
      </Link>
    })
  
    
    return (
      <section className="section">
        <div className="container">
          <div className="box bc-box">
            <div className="columns">
              <div className="column">
                <h1 className={`title is-4`}>Список пользователей</h1>

                <div className={styles.userListCart}>
                  <div className={`${styles.userType} ${styles.UserItem} ${styles.title}`}>Тел. номер</div>
                  <div className={`${styles.userSumm} ${styles.UserItem} ${styles.title}`}>Статус пользователя</div>
                  <div className={`${styles.userSmsCode} ${styles.UserItem} ${styles.title}`}>Дата</div>
                </div>

                {orders}
                
              </div>

            </div>

          </div>

        </div>

      </section>
    )
  }
}

AdminUserList.propTypes = {
  users: PropTypes.array,
  apiSet: PropTypes.bool,
  adminGetUsers: PropTypes.func
}

const mapStateToProps = state => ({
  users: state.admin.users,
  apiSet: state.session.apiSet
})

export default withRouter(connect(mapStateToProps, {
  adminGetUsers
})(AdminUserList))
import React, { Component } from 'react'
import { Route, Redirect, Link, withRouter } from 'react-router-dom'
import Topbar from '../Topbar'
import Home from '../Home'
import NewOrder from '../NewOrder'
import Register from '../Register'
import Verification from '../Verification'
import PhotoVerification from '../PhotoVerification'
import AdminOrder from '../AdminOrder'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Login from '../Login'
import Order from '../Order'
import UserAgreement from '../UserAgreement'
import OrdersHistory from '../OrdersHistory'
import OutOfHours from '../OutOfHours'
import AdminUserList from '../AdminUserList'
import AdminUserInfo from '../AdminUserInfo'
import AdminOrdersList from '../AdminOrderList'

import { kaspiBTCActions } from '../../redux/kaspiBTC'
import { btcKaspiActions } from '../../redux/btcKaspi'
import { cashBTCActions } from '../../redux/cashBTC'
import { btcCashActions } from '../../redux/btcCash'
import socket from '../../socket'
import { sessionActions } from '../../redux/session'
import { settingsActions } from '../../redux/settings'

export class Main extends Component {
  constructor() {
    super()

    this.state = {
      returnUrl: null
    }
  }

  componentDidMount() {
    if (socket.disconnected) {
      socket.open()
    }

    socket.on('connect', () => {
      this.props.saveSocket(socket.id)

      socket.on('price', res => {
        this.props.updateKaspiBTCPrices(res)
        this.props.updateBTCKaspiPrices(res)
        this.props.updateCashBTCPrices(res)
        this.props.updateBTCCashPrices(res)
      })
    })

    this.props.getHours()
  }

  componentWillUnmount() {
    socket.close()
  }

  componentDidUpdate() {
  }

  authorized(comp) {
    if (this.props.token) {
      return comp
    }

    if (!this.state.returnUrl) {
      this.setState(state => ({ ...state, returnUrl: this.props.location.pathname }))
    }

    const returnUrl = this.state.returnUrl ? this.state.returnUrl : this.props.location.pathname

    const location = {
      pathname: '/login',
      state: {
        returnUrl
      }
    }

    return () => <Redirect to={location} />
  }

  phoneVerified(comp) {
    if (this.props.token && this.props.phoneVerified) {
      return comp
    }

    if (!this.state.returnUrl) {
      this.setState(state => ({ ...state, returnUrl: this.props.location.pathname }))
    }

    return () => <Redirect to="/verification" />
  }

  verified(comp) {
    if (this.props.token && this.props.phoneVerified && this.props.photoVerifyStatus === 'accepted') {
      return comp
    }

    if (!this.state.returnUrl) {
      this.setState(state => ({ ...state, returnUrl: this.props.location.pathname }))
    }

    return () => <Redirect to="/photo-verification" />
  }

  isAdmin(comp) {
    if (this.props.token && this.props.phoneVerified && this.props.photoVerifyStatus === 'accepted' && this.props.isAdmin) {
      return comp
    }

    if (!this.state.returnUrl) {
      this.setState(state => ({ ...state, returnUrl: this.props.location.pathname }))
    }

    return () => <Redirect to="/" />
  }

  isWorkingHours(comp) {
    const possibleTypes = ['cashbtc', 'btccash']
    
    if (this.props.workingHours || possibleTypes.some(type => type === this.props.type)) {
      return comp
    }

    return OutOfHours
  }

  render() {
    return <div>
      <Topbar />
      
      <Route path="/" exact render={() => <Redirect to="/home/kaspibtc" />} />
      <Route path="/home" component={Home} />
      <Route path="/new-order" component={this.isWorkingHours(this.verified(NewOrder))} />
      <Route path="/order/:id" component={this.verified(Order)} />
      <Route path="/register" component={Register} />
      <Route path="/verification" component={this.authorized(Verification)} />
      <Route path="/photo-verification" component={this.phoneVerified(PhotoVerification)} />
      <Route path="/login" component={Login} />
      <Route path="/user-agreement" component={UserAgreement} />
      <Route path="/orders-history" component={this.verified(OrdersHistory)} />
      <Route path="/admin/order/:id" component={this.isAdmin(AdminOrder)} />
      <Route path="/admin/users" component={this.isAdmin(AdminUserList)} />
      <Route path="/admin/user/:phone" component={this.isAdmin(AdminUserInfo)} />
      <Route path="/admin/orders" component={this.isAdmin(AdminOrdersList)} />

      <footer className={`footer`}>
        <div className={`container has-text-centered`}>
          <label className={`label`}>BitChange. 2019</label>
          <label className={`label`}>г. Алматы, пр. аль-Фараби, 21, AFD Plaza, 9 блок, офис 214</label>
          <a className={`button is-text`} href="tel:+77755554405"><strong>+7 775 555 44 05</strong></a>
          <br />
          <Link className={`button is-text`} to="/user-agreement">Пользовательское соглашение</Link>
        </div>
      </footer>
    </div>

  }
}

Main.propTypes = {
  phoneVerified: PropTypes.bool,
  photoVerifyStatus: PropTypes.string,
  token: PropTypes.string,
  location: PropTypes.object,
  isAdmin: PropTypes.bool,
  updateKaspiBTCPrices: PropTypes.func,
  updateBTCKaspiPrices: PropTypes.func,
  updateCashBTCPrices: PropTypes.func,
  updateBTCCashPrices: PropTypes.func,
  saveSocket: PropTypes.func,
  type: PropTypes.string,
  getHours: PropTypes.func,
  workingHours: PropTypes.bool
}

const mapStateToProps = state => ({
  token: state.session.token,
  phoneVerified: state.session.phoneVerified,
  photoVerifyStatus: state.session.photoVerifyStatus,
  isAdmin: state.session.isAdmin,
  type: state.order.type,
  workingHours: state.settings.workingHours
})

export default withRouter(connect(mapStateToProps, {
  updateKaspiBTCPrices: kaspiBTCActions.updatePrices,
  updateBTCKaspiPrices: btcKaspiActions.updatePrices,
  updateCashBTCPrices: cashBTCActions.updatePrices,
  updateBTCCashPrices: btcCashActions.updatePrices,
  saveSocket: sessionActions.saveSocket,
  getHours: settingsActions.getHours
})(Main))
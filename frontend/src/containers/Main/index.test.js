/* eslint-env jest */

import React from 'react'
import { Main } from './index'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

test('renders without crashing', () => {
  const location = {
    pathname: ''
  }

  const component = shallow(
    <Main
      location={location}
      saveSocket={jest.fn}
      updateKaspiBTCPrices={jest.fn}
      updateBTCKaspiPrices={jest.fn}
      updateCashBTCPrices={jest.fn}
      updateBTCCashPrices={jest.fn}
      getHours={jest.fn}
    />
  )
  expect(component.debug()).toMatchSnapshot()
})

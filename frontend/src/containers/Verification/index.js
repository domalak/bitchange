import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import FormButton from '../../components/FormButton'
import Input from '../../components/Input'
import { verifyUserAction} from '../../redux/session/actions'

export class Verification extends Component {
  constructor() {
    super()
    this.state = {
      smsCode: '',
      error: ''
    }
    this.onHandleVerification = this.onHandleVerification.bind(this)
    this.onCodeChange = this.onCodeChange.bind(this)
  }

  onHandleVerification(){
    const smsCode = this.state.smsCode
    
    this.props.verifyUserAction({ smsCode })
  }
  onCodeChange(event) {
    event.preventDefault()
    this.setState({ smsCode: event.target.value })
  }
  componentDidMount() {
    if (this.props.phoneVerified) {
      this.props.history.push('/photo-verification')
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.phoneVerified) {
      this.props.history.push('/photo-verification')
    }
    if(this.props.errors && this.props.errors !== prevProps.errors ){
      this.setState({ error: this.props.errors.smsCode })
    }
  }

  render() {
    return (
      <section className="section">
        <div className="container">
          <div className="box bc-box">
            <div className="columns">
              <div className="column">
                <h1 className={`title is-4`}>Подтверждение по СМС</h1>
                <h2 className={`subtitle is-5 bc-subtitle`}>По указанному Вами номеру было отправлено СМС с кодом, пожалуйста введите его.</h2>
                <Input
                  type="smsCode"
                  label={`Код из смс`}
                  onChange={this.onCodeChange}
                  error={this.state.error}
                />    
                <FormButton label="К последнему шагу" onClick={this.onHandleVerification} />                
              </div>
              <div className="column"></div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

Verification.propTypes = {
  verifyUserAction: PropTypes.func,
  phoneVerified: PropTypes.bool,
  history: PropTypes.object,
  errors: PropTypes.string
}

const mapStateToProps = state => ({
  phoneVerified: state.session.phoneVerified,
  errors: state.session.errors ? state.session.errors : ''
})

export default withRouter(connect(mapStateToProps, { verifyUserAction })(Verification))
import React from 'react'
import styles from './index.module.scss'

export const userStatus = (phoneVerified, photoVerifyStatus, isAdmin) => {
    if(isAdmin){
        return <p className={styles.statusAdmin}>Админ</p>
    }
    
    if(phoneVerified){
        if(photoVerifyStatus === 'accepted'){
            return <p className={styles.statusReady}>Готовый пользователь</p>    
        }
        if(photoVerifyStatus === 'processing'){
            return <p className={styles.status}>Нуждается в проверке</p>
        }
        if(photoVerifyStatus === 'denied'){
            return <p className={styles.statusDenied}>Фото отклонены</p>
        }
        return <p className={styles.noNeedToCheck}>Фото не прикреплены</p>
    }
    return <p className={styles.noNeedToCheck}>Ожидает ввода СМС</p>
}

export const userPhone = (phone) => {
    if(phone){
        return `+7 (${phone.substring(0, 3)}) ${phone.substring(3, 6)} ${phone.substring(6, 10)}`    
    }
}
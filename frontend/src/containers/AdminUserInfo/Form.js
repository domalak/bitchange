import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { userActions } from '../../redux/user'
import styles from './index.module.scss'
import { host } from '../../local'
import axios from 'axios'
import { userStatus, userPhone } from './functions'

export class Form extends Component {
  constructor() {
    super()
    this.state = {
      selectedStatus: null,
      denyReason: null
    }

    this.onSelectHandle = this.onSelectHandle.bind(this)
    this.buttonCheck = this.buttonCheck.bind(this)
    this.onReasonHandle = this.onReasonHandle.bind(this)
  }

  componentDidMount() {
    if (this.props.apiSet) {
      this.props.getUser({ phone: this.props.match.params.phone })
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.apiSet && this.props.apiSet !== prevProps.apiSet) {
      this.props.getUser({ phone: this.props.match.params.phone })
    }
  }

  buttonCheck() {
    const data = {
      phone: this.props.phone,
      selectedStatus: this.state.selectedStatus,
      denyReason: this.state.denyReason
    }
    // const selectedStatus = this.state.selectedStatus
    axios.post(`${host}/adminChangeStatus`, data)
  }

  onSelectHandle(event) {
    this.setState({ selectedStatus: event.target.value })
  }

  onReasonHandle(event) {
    this.setState({ denyReason: event.target.value })
  }

  render() {
    return (
      <div>
        <h1 className={`title is-4`}>Карточка пользователя</h1>
        <h2 className={`subtitle is-5 bc-subtitle`}>Здесь Вы можете редактировать права пользователя.</h2>

        <div className={styles.userInfo}>
          <div className={styles.phone}>Телефонный номер:
            <div className={styles.phoneSelected}>{userPhone(this.props.phone)}</div>
          </div>
          <div className={styles.status1}>Статус: {this.props.phoneVerified}
            <div className={styles.status}>{userStatus(this.props.phoneVerified, this.state.status, this.props.isAdmin)}</div>
          </div>
          <div>Дата: {this.props.createDate}</div>
        </div>

        <div className={styles.button}>
          <select value={this.state.selectedStatus} onChange={this.onSelectHandle}>
            <option value="processing">Подлежит проверке</option>
            <option value="accepted">Одобрено</option>
            <option value="denied">Отклонено</option>
          </select>
          <button className="button is-primary" onClick={this.buttonCheck} disabled={this.state.selectedStatus === 'denied' && !this.state.denyReason}>Изменить статус</button>
        </div>

        <div className={styles.reason}>
          <div className={styles.phone}>Причина отклонения заявки:

          </div>
          <input
            type="text"
            className="input"
            onChange={this.onReasonHandle}
          />
        </div>

      </div>
    )
  }
}

Form.propTypes = {
  match: PropTypes.object,
  phone: PropTypes.string,
  phoneVerified: PropTypes.bool,
  isAdmin: PropTypes.bool,
  createDate: PropTypes.string,
  getUser: PropTypes.func,
  apiSet: PropTypes.bool
}

const mapStateToProps = state => ({
  phone: state.user.phone,
  status: state.user.status,
  isAdmin: state.user.isAdmin,
  smsCode: state.user.smsCode,
  phoneVerified: state.user.phoneVerified,
  apiSet: state.session.apiSet
})

export default withRouter(connect(mapStateToProps, {
  getUser: userActions.get
})(Form))
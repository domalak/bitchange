/* eslint-env jest */

import React from 'react'
import { AdminUserInfo } from './index'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

it('renders without crashing', () => {
  const match = {
    params: {
      phone: ''
    }
  }

  const component = shallow(
    <AdminUserInfo match={match} />
  )
  expect(component.debug()).toMatchSnapshot()
})

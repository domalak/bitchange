import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { orderActions } from '../../redux/order'
import Form from './Form'
import styles from './index.module.scss'
import { host } from '../../local'

export class AdminUserInfo extends Component {
  constructor(props) {
    super(props)

    this.state = {
      photoToggled: false,
      photo1Toggled: false
    }
    this.onFirstPhoto = this.onFirstPhoto.bind(this)
    this.onSecondPhoto = this.onSecondPhoto.bind(this)
  }

  onFirstPhoto() {
    if (this.state.photoToggled) {
      return this.setState({ photoToggled: false })
    }
    this.setState({ photoToggled: true })
  }

  onSecondPhoto() {
    if (this.state.photo1Toggled) {
      return this.setState({ photo1Toggled: false })
    }
    this.setState({ photo1Toggled: true })
  }

  render() {
    return (
      <section className="section">
        <div className="container">
          <div className="box bc-box">
            <div className="columns">
              <div className="column">
                <Form />
              </div>

              <div className="column has-text-left">
                <h2 className="title is-4">Прикрепленные фотографи:</h2>
                <div className={this.state.photoToggled ? styles.firstPhotoBig : styles.firstPhoto} onClick={this.onFirstPhoto}>
                  <img src={`${host}/adminGetPhoto/id/${this.props.match.params.phone}`} alt="id" />
                </div>
                <div className={this.state.photo1Toggled ? styles.secondPhotoBig : styles.secondPhoto} onClick={this.onSecondPhoto}>
                  <img src={`${host}/adminGetPhoto/self/${this.props.match.params.phone}`} alt="self" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

AdminUserInfo.propTypes = {
  price: PropTypes.number,
  orderNumber: PropTypes.number,
  amountKZT: PropTypes.number,
  amountBTC: PropTypes.number,
  type: PropTypes.string,
  date: PropTypes.string,
  getOrder: PropTypes.func,
  history: PropTypes.object,
  token: PropTypes.string,
  status: PropTypes.string,
  match: PropTypes.object,
  apiSet: PropTypes.bool
}

const mapStateToProps = state => ({
  price: state.order.price,
  amountKZT: state.order.amountKZT,
  amountBTC: state.order.amountBTC,
  orderNumber: state.order.orderNumber,
  type: state.order.type,
  date: state.order.date,
  token: state.session.token,
  status: state.order.status,
  apiSet: state.session.apiSet
})

export default withRouter(connect(mapStateToProps, {
  getOrder: orderActions.get
})(AdminUserInfo))
/* eslint-env jest */

import React from 'react'
import { Form } from './Form'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

it('renders without crashing', () => {
  const component = shallow(
    <Form />
  )
  expect(component.debug()).toMatchSnapshot()
})

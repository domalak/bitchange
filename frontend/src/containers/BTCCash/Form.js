import React, { Component } from 'react'
import cashImg from '../../assets/images/cash.png'
import bitcoinImg from '../../assets/images/bitcoin.png'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { btcCashActions } from '../../redux/btcCash'
import { withRouter } from 'react-router-dom'
import { orderActions } from '../../redux/order'
import Input from '../../components/Input'
import FormButton from '../../components/FormButton'

export class Form extends Component {
  constructor(props) {
    super(props)

    this.updateBTC = this.updateBTC.bind(this)
    this.updateKZT = this.updateKZT.bind(this)
    this.onKZTFocus = this.onKZTFocus.bind(this)
    this.onKZTBlur = this.onKZTBlur.bind(this)
    this.onBTCFocus = this.onBTCFocus.bind(this)
    this.onBTCBlur = this.onBTCBlur.bind(this)
    this.createNewOrder = this.createNewOrder.bind(this)
  }

  updateBTC(values) {
    if (this.props.btcFocused) {
      this.props.updateBTC(values.floatValue)
    }
  }

  updateKZT(values) {
    if (this.props.kztFocused) {
      this.props.updateKZT(values.floatValue)
    }
  }

  onKZTFocus() {
    this.props.setFocusKZT(true)
  }

  onKZTBlur() {
    this.props.setFocusKZT(false)
  }

  onBTCFocus() {
    this.props.setFocusBTC(true)
  }

  onBTCBlur() {
    this.props.setFocusBTC(false)
  }

  createNewOrder() {
    const { amountKZT, amountBTC, price } = this.props
    const type = 'btccash'

    this.props.createNewOrder({ amountKZT, amountBTC, price, type })

    this.props.history.push('/new-order')
  }

  render() {
    const amountKZT = this.props.amountKZT
    const amountBTC = this.props.amountBTC

    return (
      <div>
        <h1 className={`title is-4`}>Обмен BTC <span role="img" aria-label="finger-right">👉</span> Наличные</h1>
        <h2 className={`subtitle is-5 bc-subtitle`}>Обменяйте свои наличные KZT на BTC</h2>

        <Input
          type="number"
          img={bitcoinImg}
          label={`Отправляете`}
          value={amountBTC}
          decimals={5}
          suffix=" BTC"
          onFocus={this.onBTCFocus}
          onBlur={this.onBTCBlur}
          onChange={this.updateBTC}
        />

        <Input
          type="number"
          img={cashImg}
          label={`Получаете`}
          value={amountKZT}
          decimals={0}
          suffix=" тг."
          onFocus={this.onBTCFocus}
          onBlur={this.onBTCBlur}
          onChange={this.updateKZT}
        />

        <FormButton label="Далее" onClick={this.createNewOrder} />
      </div>
    )
  }
}

Form.propTypes = {
  price: PropTypes.number,
  amountKZT: PropTypes.number,
  amountBTC: PropTypes.number,
  kztFocused: PropTypes.bool,
  btcFocused: PropTypes.bool,
  updateKZT: PropTypes.func,
  updateBTC: PropTypes.func,
  setFocusKZT: PropTypes.func,
  setFocusBTC: PropTypes.func,
  createNewOrder: PropTypes.func,
  history: PropTypes.object
}

const mapStateToProps = state => ({
  price: state.btcCash.price,
  amountKZT: state.btcCash.amountKZT,
  amountBTC: state.btcCash.amountBTC,
  kztFocused: state.btcCash.kztFocused,
  btcFocused: state.btcCash.btcFocused
})

export default withRouter(connect(mapStateToProps, {
  updateKZT: btcCashActions.updateKZT,
  updateBTC: btcCashActions.updateBTC,
  setFocusKZT: btcCashActions.setFocusKZT,
  setFocusBTC: btcCashActions.setFocusBTC,
  createNewOrder: orderActions.create
})(Form))
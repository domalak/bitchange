import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import logoImg from '../../assets/images/logo1.png'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { logout } from '../../redux/session/actions'

export class Topbar extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isActive: false
    }

    this.onBurgerClick = this.onBurgerClick.bind(this)
    this.menuButtons = this.menuButtons.bind(this)
  }

  logout(e) {
    e.preventDefault()
    this.props.logout()
  }

  menuButtons() {
    if(this.props.token){
      if(this.props.isAdmin){ //ссылки для админа
        return <div className="navbar-end">
          <Link className="navbar-item" to="/admin/orders" onClick={this.onBurgerClick}>
            Список заказов
          </Link>
          <Link className="navbar-item" to="/admin/users" onClick={this.onBurgerClick}>
            Список пользователей
          </Link>
          <a className="navbar-item" onClick={this.logout.bind(this)} href="/">
            Выйти
          </a>
        </div>
      }
      //ссылки для простого пользователя:
      return <div className="navbar-end"> 
        <Link className="navbar-item" to="/orders-history" onClick={this.onBurgerClick}>
          История заказов
        </Link>
        <a href="/" className="navbar-item" onClick={this.logout.bind(this)}>
          Выйти
        </a>
      </div>
    }
    //ссылки для гостя:
    return <div className="navbar-end">
      <Link className="navbar-item" to="/login" onClick={this.onBurgerClick}>
        Вход
      </Link>
      <Link className="navbar-item" to="/register" onClick={this.onBurgerClick}>
        Регистрация
      </Link>
    </div>
  }

  onBurgerClick() {
    this.setState(state => ({ ...state, isActive: !this.state.isActive }))
  }

  render() {
    return (
      <nav className={`navbar`}>
        <div className="container">
          <div className="navbar-brand">
            <Link className={`navbar-item`} to="/">
              <img src={logoImg} alt="Logo" />
            </Link>

            <button className={`navbar-burger burger ${this.state.isActive ? 'is-active' : null} button is-white`} onClick={this.onBurgerClick}>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </button>
          </div>

          <div className={`navbar-menu ${this.state.isActive ? 'is-active' : null}`}>
            <div className="navbar-start">
            </div>

            {/* Генерация меню в зависимости от роли пользователя*/}
            {this.menuButtons()}
          </div>
        </div>
      </nav>
    )
  }
}

Topbar.propTypes = {
  token: PropTypes.string,
  logout: PropTypes.func,
  isAdmin: PropTypes.bool
}

const mapStateToProps = state => ({
  token: state.session.token,
  isAdmin: state.session.isAdmin
})

export default connect(mapStateToProps, {
  logout
})(Topbar)
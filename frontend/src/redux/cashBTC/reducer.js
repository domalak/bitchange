import {
  CASHBTC_UPDATE_PRICES,
  CASHBTC_UPDATE_KZT,
  CASHBTC_UPDATE_BTC,
  CASHBTC_SET_FOCUS_KZT,
  CASHBTC_SET_FOCUS_BTC
} from "./types"

const initialState = {
  price: 0,
  amountKZT: 100000,
  amountBTC: 0,
  dates: [],
  rates: [],
  kztFocused: false,
  btcFocused: false
}

let amountBTC
let amountKZT

export default (state = initialState, action) => {
  switch (action.type) {
    case CASHBTC_UPDATE_PRICES:
      amountBTC = state.amountBTC
      amountKZT = state.amountKZT

      if (state.btcFocused) {
        amountKZT = state.amountBTC * action.payload.cashBTCPrice
      }
      else {
        amountBTC = action.payload.cashBTCPrice ? state.amountKZT / action.payload.cashBTCPrice : 0

        if (amountBTC < 0.00001) {
          amountBTC = 0
        }
      }

      return {
        ...state,
        price: action.payload.cashBTCPrice,
        dates: action.payload.dates,
        rates: action.payload.cashBTCData,
        percent: action.payload.cashBTCPercent,
        amountBTC,
        amountKZT
      }
    case CASHBTC_UPDATE_KZT:
      amountBTC = state.price ? action.payload / state.price : 0

      if (amountBTC < 0.00001) {
        amountBTC = 0
      }

      return {
        ...state,
        amountKZT: action.payload,
        amountBTC
      }
    case CASHBTC_UPDATE_BTC:
      return {
        ...state,
        amountBTC: action.payload,
        amountKZT: state.price * action.payload
      }
    case CASHBTC_SET_FOCUS_KZT:
      return {
        ...state,
        kztFocused: action.payload
      }
    case CASHBTC_SET_FOCUS_BTC:
      return {
        ...state,
        btcFocused: action.payload
      }
    default:
      return state
  }
}
import {
  CASHBTC_UPDATE_PRICES,
  CASHBTC_UPDATE_KZT,
  CASHBTC_UPDATE_BTC,
  CASHBTC_SET_FOCUS_BTC,
  CASHBTC_SET_FOCUS_KZT
} from './types'

export const updatePrices = payload => ({
  type: CASHBTC_UPDATE_PRICES,
  payload
})

export const updateKZT = payload => ({
  type: CASHBTC_UPDATE_KZT,
  payload
})

export const updateBTC = payload => ({
  type: CASHBTC_UPDATE_BTC,
  payload
})

export const setFocusKZT = payload => ({
  type: CASHBTC_SET_FOCUS_KZT,
  payload
})

export const setFocusBTC = payload => ({
  type: CASHBTC_SET_FOCUS_BTC,
  payload
})
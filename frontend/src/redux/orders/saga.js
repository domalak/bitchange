import {
  takeLatest,
  put,
  call
} from 'redux-saga/effects'
import * as types from './types'
import {
  orderAPI
} from '../../api'

function* getOrdersHistory(action) {
  try {
    const response = yield call(orderAPI.getOrdersHistory, action.payload)
    yield put({
      type: types.GET_ORDERS_HISTORY_SUCCESS,
      payload: response.data
    })
  } catch (err) {
    yield put({
      type: types.GET_ORDERS_HISTORY_ERROR,
      payload: err.response.data.errors
    })
  }
}

export default function* ordersSaga() {
  yield takeLatest(types.GET_ORDERS_HISTORY, getOrdersHistory)
}
import * as types from './types'

const initialState = {
  errors: {}
}

export default function (state = initialState, action) {
  const payload = action.payload

  switch (action.type) {
    case types.GET_ORDERS_HISTORY:
      return { ...state }
    case types.GET_ORDERS_HISTORY_SUCCESS:
      return { ...state, ...payload }
    case types.GET_ORDERS_HISTORY_ERROR:
      return { ...state, errors: payload }
    
    default:
      return state
  }
}
import * as types from './types'

export const getOrdersHistory = (payload) => {
  return {
    type: types.GET_ORDERS_HISTORY,
    payload
  }
}
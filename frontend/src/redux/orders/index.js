import * as ordersActions from './actions'
import ordersReducer from './reducer'

export { ordersActions, ordersReducer }
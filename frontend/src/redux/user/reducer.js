import * as types from './types'

const initialState = {
  errors: {}
}

export default function (state = initialState, action) {
  const payload = action.payload

  switch (action.type) {
    case types.GET_USER:
      return { ...state, payload }
    case types.GET_USER_SUCCESS:
      return {
        ...payload,
        token: state.token,
        apiSet: state.apiSet,
        socketId: state.socketId
      }
    case types.GET_USER_ERROR:
      return { ...state, errors: payload }
    default:
      return state
  }
}
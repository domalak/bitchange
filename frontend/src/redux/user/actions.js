import * as types from './types'

export const get = payload => ({
  type: types.GET_USER,
  payload
})
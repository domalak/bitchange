import {
  put,
  call,
  takeEvery
} from 'redux-saga/effects'
import * as types from './types'
import {
  userAPI
} from '../../api'
import { userActions } from './'

function* get(action) {
  try {
    const response = yield call(userAPI.get, action.payload)
    yield put({
      type: types.GET_USER_SUCCESS,
      payload: response.data
    })
  } catch (error) {
    if (error.response.status === 401) {
      return yield put(userActions.logout())
    }

    yield put({
      type: types.GET_USER_ERROR,
      payload: error
    })
  }
}

export default function* userSaga() {
  yield takeEvery(types.GET_USER, get)
}
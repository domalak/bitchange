import * as types from './types'

export const adminGetUsers = (user) => {
  return {
    type: types.GET_USERS_LIST,
    user
  }
}

export const adminGetOrders = (user) => {
  return {
    type: types.GET_ADMIN_ORDERS,
    user
  }
}

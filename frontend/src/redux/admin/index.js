import * as adminActions from './actions'
import adminReducer from './reducer'

export { adminActions, adminReducer }
export const GET_USERS_LIST = 'GET_USERS_LIST'
export const GET_USERS_LIST_SUCCESS = 'GET_USERS_LIST_SUCCESS'
export const GET_USERS_LIST_ERROR = 'GET_USERS_LIST_ERROR'

export const GET_ADMIN_ORDERS = 'GET_ADMIN_ORDERS'
export const GET_ADMIN_ORDERS_SUCCESS = 'GET_ADMIN_ORDERS_SUCCESS'
export const GET_ADMIN_ORDERS_ERROR = 'GET_ADMIN_ORDERS_ERROR'



import {
  takeLatest,
  put,
  call
} from 'redux-saga/effects'
import * as types from './types'
import {
  adminAPI
} from '../../api'


function* adminGetUsers(action) {
  try {
    const response = yield call(adminAPI.adminGetUsers, action.payload)
    yield put({
      type: types.GET_USERS_LIST_SUCCESS,
      payload: response.data
    })
  } catch (err) {
    yield put({
      type: types.GET_USERS_LIST_ERROR,
      payload: err.response.data.errors
    })
  }
}

function* adminGetOrders(action) {
  try {
    const response = yield call(adminAPI.adminGetOrders, action.user)
    yield put({
      type: types.GET_ADMIN_ORDERS_SUCCESS,
      payload: response.data
    })
  } catch (err) {
    yield put({
      type: types.GET_ADMIN_ORDERS_ERROR,
      payload: err.response.data.errors
    })
  }
}

export default function* adminSaga() {
  yield takeLatest(types.GET_USERS_LIST, adminGetUsers)
  yield takeLatest(types.GET_ADMIN_ORDERS, adminGetOrders)
}
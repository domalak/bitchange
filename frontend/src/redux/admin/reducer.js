import * as types from './types'

const initialState = {
  errors: {},
  user: {}
}

export default function (state = initialState, action) {
  const payload = action.payload

  switch (action.type) {
    case types.GET_USERS_LIST:
      return { ...state }
    case types.GET_USERS_LIST_SUCCESS:
      return { ...state, ...payload }
    case types.GET_USERS_LIST_ERROR:
      return { ...state, errors: payload, users: null }
    case types.GET_ADMIN_ORDERS:
      return { ...state }
    case types.GET_ADMIN_ORDERS_SUCCESS:
      return { ...state, ...payload }
    case types.GET_ADMIN_ORDERS_ERROR:
      return { ...state, errors: payload, orders: null }
    default:
      return state
  }
}
import orderSaga from './order/saga'
import userSaga from './user/saga'
import ordersSaga from './orders/saga'
import adminSaga from './admin/saga'
import settingsSaga from './settings/saga'
import sessionSaga from './session/saga'

export default [
  orderSaga, userSaga, ordersSaga, adminSaga, settingsSaga, sessionSaga
]
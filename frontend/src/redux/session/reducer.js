import * as types from './types'

const initialState = {
  errors: {}
}

export default function (state = initialState, action) {
  const payload = action.payload

  switch (action.type) {
    case types.REGISTER_USER_SUCCESS:
      return { ...payload }
    case types.REGISTER_USER_ERROR:
      return { ...state, errors: payload }
    case types.LOGOUT_USER:
      return {}
    case types.LOGIN_USER_SUCCESS:
      return { ...state, ...payload }
    case types.LOGIN_USER_ERROR:
      return { ...state, errors: payload }
    case types.GET_SESSION:
      return { ...state, payload }
    case types.GET_SESSION_SUCCESS:
      return {
        ...payload,
        token: state.token,
        apiSet: state.apiSet,
        socketId: state.socketId
      }
    case types.GET_SESSION_ERROR:
      return { ...state, errors: payload }
    case types.VERIFY_USER_SUCCESS:
      return { ...state, ...payload }
    case types.VERIFY_USER_ERROR:
      return { ...state, errors: payload }
    case types.SET_API_AUTH:
      return { ...state, apiSet: payload }
    case types.SAVE_SOCKET:
      return { ...state, socketId: payload }
    case types.UPLOAD_USER_PHOTOS_SUCCESS:
      return { ...state, ...payload }
    case types.UPLOAD_USER_PHOTOS_ERROR:
      return { ...state, error: payload }
    default:
      return state
  }
}
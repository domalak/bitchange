import {
  takeLatest,
  put,
  call,
  takeEvery
} from 'redux-saga/effects'
import * as types from './types'
import {
  userAPI
} from '../../api'
import { sessionActions } from './'

function* register(action) {
  try {
    const response = yield call(userAPI.register, action.user)
    yield put({
      type: types.REGISTER_USER_SUCCESS,
      payload: response.data
    })
  } catch (error) {
    yield put({
      type: types.REGISTER_USER_ERROR,
      payload: error.response.data.errors
    })
  }
}

function* login(payload) {
  try {
    const response = yield call(userAPI.login, payload)
    yield put({
      type: types.LOGIN_USER_SUCCESS,
      payload: response.data
    })
  } catch (error) {
    yield put({
      type: types.LOGIN_USER_ERROR,
      payload: error.response.data.errors
    })
  }
}

function* get(action) {
  try {
    const response = yield call(userAPI.get, action.payload)
    yield put({
      type: types.GET_SESSION_SUCCESS,
      payload: response.data
    })
  } catch (error) {
    if (error.response.status === 401) {
      return yield put(sessionActions.logout())
    }

    yield put({
      type: types.GET_SESSION_ERROR,
      payload: error
    })
  }
}

function* verify(action) {
  try {
    const response = yield call(userAPI.verify, action.payload)
    yield put({
      type: types.VERIFY_USER_SUCCESS,
      payload: response.data
    })
  } catch (error) {
    yield put({
      type: types.VERIFY_USER_ERROR,
      payload: error.response.data.errors
    })
  }
}

function* uploadPhotos(action) {
  try {
    const response = yield call(userAPI.uploadPhotos, action.payload)
    yield put({
      type: types.UPLOAD_USER_PHOTOS_SUCCESS,
      payload: response.data
    })
  } catch (error) {
    yield put({
      type: types.UPLOAD_USER_PHOTOS_ERROR,
      payload: error.response.data
    })
  }
}

export default function* sessionSaga() {
  yield takeLatest(types.REGISTER_USER, register)
  yield takeLatest(types.LOGIN_USER, login)
  yield takeEvery(types.GET_SESSION, get)
  yield takeLatest(types.VERIFY_USER, verify)
  yield takeLatest(types.UPLOAD_USER_PHOTOS, uploadPhotos)
}
import * as types from './types'

export const registerUserAction = (user) => {
  return {
    type: types.REGISTER_USER,
    user
  }
}

export const loginUserAction = (user) => {
  return {
    type: types.LOGIN_USER,
    user
  }
}

export const logout = (user) => {
  return {
    type: types.LOGOUT_USER,
    user
  }
}

export const get = payload => ({
  type: types.GET_SESSION,
  payload
})

export const verifyUserAction = payload => {
  return {
    type: types.VERIFY_USER,
    payload
  }
}

export const setAPIAuth = payload => ({
  type: types.SET_API_AUTH,
  payload
})

export const saveSocket = payload => ({
  type: types.SAVE_SOCKET,
  payload
})

export const uploadPhotos = payload => ({
  type: types.UPLOAD_USER_PHOTOS,
  payload
})

import { combineReducers } from 'redux'
import { kaspiBTCReducer } from './kaspiBTC'
import { btcKaspiReducer } from './btcKaspi'
import { cashBTCReducer } from './cashBTC'
import { btcCashReducer } from './btcCash'
import { orderReducer } from './order'
import { userReducer } from './user'
import { ordersReducer } from './orders'
import { adminReducer } from './admin'
import { settingsReducer } from './settings'
import { sessionReducer } from './session'

export default combineReducers({ 
  kaspiBTC: kaspiBTCReducer,
  btcKaspi: btcKaspiReducer,
  order: orderReducer,
  user: userReducer,
  cashBTC: cashBTCReducer,
  btcCash: btcCashReducer,
  orders: ordersReducer,
  admin: adminReducer,
  settings: settingsReducer,
  session: sessionReducer
})
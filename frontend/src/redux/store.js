import { createStore, applyMiddleware } from 'redux'
import reducers from './reducers'
import createSagaMiddleware from 'redux-saga'
import sagas from './sagas'
import { composeWithDevTools } from 'redux-devtools-extension'
import throttle from 'lodash/throttle'
import { env } from '../local'

const sagaMiddleware = createSagaMiddleware()
let middleware

if (env === 'dev') {
  middleware = composeWithDevTools(
    applyMiddleware(sagaMiddleware)
  )
} else {
  middleware = applyMiddleware(sagaMiddleware)
}

let initialState = localStorage.getItem('redux_state') ? JSON.parse(localStorage.getItem('redux_state')) : {}

if (initialState.session) {
  initialState.session.apiSet = false
  initialState.session.socketId = null
}

const store = createStore(reducers, initialState, middleware)

sagas.forEach(saga => sagaMiddleware.run(saga))

store.subscribe(throttle(() => localStorage.setItem('redux_state', JSON.stringify(store.getState())), 100))

export default store
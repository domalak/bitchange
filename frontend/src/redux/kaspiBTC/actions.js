import {
  KASPIBTC_UPDATE_PRICES,
  KASPIBTC_UPDATE_KZT,
  KASPIBTC_UPDATE_BTC,
  KASPIBTC_SET_FOCUS_BTC,
  KASPIBTC_SET_FOCUS_KZT
} from './types'

export const updatePrices = payload => ({
  type: KASPIBTC_UPDATE_PRICES,
  payload
})

export const updateKZT = payload => ({
  type: KASPIBTC_UPDATE_KZT,
  payload
})

export const updateBTC = payload => ({
  type: KASPIBTC_UPDATE_BTC,
  payload
})

export const setFocusKZT = payload => ({
  type: KASPIBTC_SET_FOCUS_KZT,
  payload
})

export const setFocusBTC = payload => ({
  type: KASPIBTC_SET_FOCUS_BTC,
  payload
})
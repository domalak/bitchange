import {
  KASPIBTC_UPDATE_PRICES,
  KASPIBTC_UPDATE_KZT,
  KASPIBTC_UPDATE_BTC,
  KASPIBTC_SET_FOCUS_KZT,
  KASPIBTC_SET_FOCUS_BTC
} from "./types"

const initialState = {
  price: 0,
  amountKZT: 100000,
  amountBTC: 0,
  dates: [],
  rates: [],
  kztFocused: false,
  btcFocused: false
}

let amountBTC
let amountKZT

export default (state = initialState, action) => {
  switch (action.type) {
    case KASPIBTC_UPDATE_PRICES:
      amountBTC = state.amountBTC
      amountKZT = state.amountKZT

      if (state.btcFocused) {
        amountKZT = state.amountBTC * action.payload.kaspiBTCPrice
      }
      else {
        amountBTC = action.payload.kaspiBTCPrice ? state.amountKZT / action.payload.kaspiBTCPrice : 0

        if (amountBTC < 0.00001) {
          amountBTC = 0
        }
      }

      return {
        ...state,
        price: action.payload.kaspiBTCPrice,
        dates: action.payload.dates,
        rates: action.payload.kaspiBTCData,
        percent: action.payload.kaspiBTCPercent,
        amountBTC,
        amountKZT
      }
    case KASPIBTC_UPDATE_KZT:
      amountBTC = state.price ? action.payload / state.price : 0

      if (amountBTC < 0.00001) {
        amountBTC = 0
      }

      return {
        ...state,
        amountKZT: action.payload,
        amountBTC
      }
    case KASPIBTC_UPDATE_BTC:
      return {
        ...state,
        amountBTC: action.payload,
        amountKZT: state.price * action.payload
      }
    case KASPIBTC_SET_FOCUS_KZT:
      return {
        ...state,
        kztFocused: action.payload
      }
    case KASPIBTC_SET_FOCUS_BTC:
      return {
        ...state,
        btcFocused: action.payload
      }
    default:
      return state
  }
}
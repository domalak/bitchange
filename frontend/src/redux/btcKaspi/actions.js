import {
  BTCKASPI_UPDATE_PRICES,
  BTCKASPI_UPDATE_KZT,
  BTCKASPI_UPDATE_BTC,
  BTCKASPI_SET_FOCUS_BTC,
  BTCKASPI_SET_FOCUS_KZT
} from './types'

export const updatePrices = payload => ({
  type: BTCKASPI_UPDATE_PRICES,
  payload
})

export const updateKZT = payload => ({
  type: BTCKASPI_UPDATE_KZT,
  payload
})

export const updateBTC = payload => ({
  type: BTCKASPI_UPDATE_BTC,
  payload
})

export const setFocusKZT = payload => ({
  type: BTCKASPI_SET_FOCUS_KZT,
  payload
})

export const setFocusBTC = payload => ({
  type: BTCKASPI_SET_FOCUS_BTC,
  payload
})
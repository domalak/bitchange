import {
  BTCKASPI_UPDATE_PRICES,
  BTCKASPI_UPDATE_KZT,
  BTCKASPI_UPDATE_BTC,
  BTCKASPI_SET_FOCUS_BTC,
  BTCKASPI_SET_FOCUS_KZT
} from "./types"

const initialState = {
  price: 0,
  amountKZT: 0,
  amountBTC: 0.01,
  dates: [],
  rates: [],
  kztFocused: false,
  btcFocused: false
}

let amountBTC
let amountKZT

export default (state = initialState, action) => {
  switch (action.type) {
    case BTCKASPI_UPDATE_PRICES:
      amountBTC = state.amountBTC
      amountKZT = state.amountKZT

      if (state.kztFocused) {
        amountBTC = action.payload.btcKaspiPrice ? state.amountKZT / action.payload.btcKaspiPrice : 0

        if (amountBTC < 0.00001) {
          amountBTC = 0
        }
      }
      else {
        amountKZT = state.amountBTC * action.payload.btcKaspiPrice
      }

      return {
        ...state,
        price: action.payload.btcKaspiPrice,
        dates: action.payload.dates,
        rates: action.payload.btcKaspiData,
        percent: action.payload.btcKaspiPercent,
        amountBTC,
        amountKZT
      }
    case BTCKASPI_UPDATE_KZT:
      amountBTC = state.price ? action.payload / state.price : 0

      if (amountBTC < 0.00001) {
        amountBTC = 0
      }

      return {
        ...state,
        amountKZT: action.payload,
        amountBTC
      }
    case BTCKASPI_UPDATE_BTC:
      return {
        ...state,
        amountBTC: action.payload,
        amountKZT: state.price * action.payload
      }
    case BTCKASPI_SET_FOCUS_KZT:
      return {
        ...state,
        kztFocused: action.payload
      }
    case BTCKASPI_SET_FOCUS_BTC:
      return {
        ...state,
        btcFocused: action.payload
      }
    default:
      return state
  }
}
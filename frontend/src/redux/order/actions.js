import * as types from './types'

export const create = payload => ({
  type: types.CREATE_ORDER,
  payload
})

export const createSuccess = payload => ({
  type: types.CREATE_ORDER_SUCCESS,
  payload
})

export const createError = payload => ({
  type: types.CREATE_ORDER_ERROR,
  payload
})

export const getPrice = payload => ({
  type: types.GET_ORDER_PRICE,
  payload
})

export const getPriceSuccess = payload => ({
  type: types.GET_ORDER_PRICE_SUCCESS,
  payload
})

export const getPriceError = payload => ({
  type: types.GET_ORDER_PRICE_ERROR,
  payload
})

export const get = payload => ({
  type: types.GET_ORDER,
  payload
})

export const getSuccess = payload => ({
  type: types.GET_ORDER_SUCCESS,
  payload
})

export const getError = payload => ({
  type: types.GET_ORDER_ERROR,
  payload
})

export const clear = payload => ({
  type: types.CLEAR_ORDER,
  payload
})

export const updateKZT = payload => ({
  type: types.ORDER_UPDATE_KZT,
  payload
})

export const updateBTC = payload => ({
  type: types.ORDER_UPDATE_BTC,
  payload
})

export const updateBTCAddress = payload => ({
  type: types.ORDER_UPDATE_BTC_ADDRESS,
  payload
})

export const updateKaspiCard = payload => ({
  type: types.ORDER_UPDATE_KASPI_CARD,
  payload
})

export const setPaymentSent = payload => ({
  type: types.SET_PAYMENT_SET,
  payload
})

export const setPaymentSentSuccess = payload => ({
  type: types.SET_PAYMENT_SET_SUCCESS,
  payload
})

export const setPaymentSentError = payload => ({
  type: types.SET_PAYMENT_SET_ERROR,
  payload
})

export const setFormValid = payload => ({
  type: types.SET_FORM_VALID,
  payload
})

export const update = payload => ({
  type: types.UPDATE_ORDER,
  payload
})

export const updateSuccess = payload => ({
  type: types.UPDATE_ORDER_SUCCESS,
  payload
})

export const updateError = payload => ({
  type: types.UPDATE_ORDER_ERROR,
  payload
})

export const updateStatus = payload => ({
  type: types.UPDATE_ORDER_STATUS,
  payload
})
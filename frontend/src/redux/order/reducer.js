import * as types from "./types"

const initialState = {
  amountKZT: 0,
  amountBTC: 0,
  price: 0
}

export default (state = initialState, action) => {
  switch (action.type) {
    case types.CREATE_ORDER:
      return {
        ...state,
        amountBTC: action.payload.amountBTC,
        amountKZT: action.payload.amountKZT
      }
    case types.CREATE_ORDER_SUCCESS:
      return {
        ...action.payload
      }
    case types.CREATE_ORDER_ERROR:
      return {
        ...state,
        status: 'error',
        error: action.payload
      }
    case types.GET_ORDER_PRICE:
      return state
    case types.GET_ORDER_PRICE_SUCCESS:
      return {
        ...state,
        price: action.payload.price,
        date: action.payload.date
      }
    case types.GET_ORDER_PRICE_ERROR:
      return {
        ...state,
        status: 'error',
        error: action.payload
      }
    case types.GET_ORDER:
      return state
    case types.GET_ORDER_SUCCESS:
      return {
        ...state,
        ...action.payload
      }
    case types.GET_ORDER_ERROR:
      return {
        ...state,
        status: 'error',
        error: action.payload
      }
    case types.CLEAR_ORDER:
      return initialState
    case types.ORDER_UPDATE_KZT:
      let amountBTC = state.price ? action.payload / state.price : 0

      if (amountBTC < 0.00001) {
        amountBTC = 0
      }

      return {
        ...state,
        amountKZT: action.payload,
        amountBTC
      }
    case types.ORDER_UPDATE_BTC:
      return {
        ...state,
        amountBTC: action.payload,
        amountKZT: state.price * action.payload
      }
    case types.ORDER_UPDATE_BTC_ADDRESS:
      return {
        ...state,
        btcAddress: action.payload
      }
    case types.ORDER_UPDATE_KASPI_CARD:
      return {
        ...state,
        kaspiCard: action.payload
      }
    case types.UPDATE_ORDER:
      return {
        ...state
      }
    case types.UPDATE_ORDER_SUCCESS:
      return {
        ...state,
        status: action.payload.status
      }
    case types.UPDATE_ORDER_ERROR:
      return {
        ...state,
        status: 'error',
        error: action.payload
      }
    case types.SET_FORM_VALID:
      return {
        ...state,
        isFormValid: action.payload
      }
    case types.SET_PAYMENT_SET:
      return state
    case types.SET_PAYMENT_SET_SUCCESS:
      return {
        ...state,
        ...action.payload
      }
    case types.SET_PAYMENT_SET_ERROR:
      return {
        ...state,
        status: 'error',
        error: action.payload
      }
    case types.UPDATE_ORDER_STATUS:
      return {
        ...state,
        status: action.payload.status
      }
    default:
      return state
  }
}
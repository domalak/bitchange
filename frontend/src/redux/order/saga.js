import {
  CREATE_ORDER,
  GET_ORDER_PRICE,
  UPDATE_ORDER,
  GET_ORDER,
  SET_PAYMENT_SET
} from "./types"
import { takeLatest, call, put } from 'redux-saga/effects'
import { orderAPI } from '../../api'
import { orderActions } from "."
import { sessionActions } from '../session'

function* create(action) {
  try {
    const res = yield call(orderAPI.create, action.payload)

    yield put(orderActions.createSuccess(res.data))
  } catch (error) {
    yield put(orderActions.createError(error))
  }
}

function* getPrice(action) {
  try {
    const res = yield call(orderAPI.getPrice, action.payload)

    yield put(orderActions.getPriceSuccess(res.data))
  } catch (error) {
    if (error.response.status === 401) {
      return yield put(sessionActions.logout())
    }
    
    return yield put(orderActions.getPriceError(error))
  }
}

function* get(action) {
  try {
    const res = yield call(orderAPI.get, action.payload)

    yield put(orderActions.getSuccess(res.data))
  } catch (error) {
    if (error.response.status === 401) {
      return yield put(sessionActions.logout())
    }

    return yield put(orderActions.getError(error))
  }
}

function* update(action) {
  try {
    const res = yield call(orderAPI.update, action.payload)

    yield put(orderActions.updateSuccess(res.data))
  } catch (error) {
    yield put(orderActions.updateError(error))
  }
}

function* setPaymentSent(action) {
  try {
    const res = yield call(orderAPI.setPaymentSent, action.payload)

    yield put(orderActions.setPaymentSentSuccess(res.data))
  } catch (error) {
    yield put(orderActions.setPaymentSentError(error))
  }
}

function* orderSaga() {
  yield takeLatest(CREATE_ORDER, create)
  yield takeLatest(GET_ORDER_PRICE, getPrice)
  yield takeLatest(GET_ORDER, get)
  yield takeLatest(UPDATE_ORDER, update)
  yield takeLatest(SET_PAYMENT_SET, setPaymentSent)
}

export default orderSaga
import {
  GET_HOURS
} from "./types"
import { takeLatest, call, put } from 'redux-saga/effects'
import { settingsAPI } from '../../api'
import { settingsActions } from "."

function* create() {
  try {
    yield call(settingsAPI.getHours)

    yield put(settingsActions.getHoursSuccess())
  } catch (error) {
    yield put(settingsActions.getHoursError())
  }
}

function* orderSaga() {
  yield takeLatest(GET_HOURS, create)
}

export default orderSaga
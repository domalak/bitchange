import * as types from "./types"

const initialState = {
  workingHours: true
}

export default (state = initialState, action) => {
  switch (action.type) {
    case types.GET_HOURS_SUCCESS:
      return {
        ...state,
        workingHours: true
      }
    case types.GET_HOURS_ERROR:
      return {
        ...state,
        workingHours: false
      }
    default:
      return state
  }
}
import * as settingsActions from './actions'
import settingsReducer from './reducer'

export { settingsActions, settingsReducer }
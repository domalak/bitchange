import * as types from './types'

export const getHours = payload => ({
  type: types.GET_HOURS,
  payload
})

export const getHoursSuccess = payload => ({
  type: types.GET_HOURS_SUCCESS,
  payload
})

export const getHoursError = payload => ({
  type: types.GET_HOURS_ERROR,
  payload
})

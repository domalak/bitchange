export const BTCCASH_UPDATE_PRICES = 'BTCCASH_UPDATE_PRICES'
export const BTCCASH_UPDATE_KZT = 'BTCCASH_UPDATE_KZT'
export const BTCCASH_UPDATE_BTC = 'BTCCASH_UPDATE_BTC'
export const BTCCASH_SET_FOCUS_KZT = 'BTCCASH_SET_FOCUS_KZT'
export const BTCCASH_SET_FOCUS_BTC = 'BTCCASH_SET_FOCUS_BTC'
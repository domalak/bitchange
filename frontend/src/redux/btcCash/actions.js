import {
  BTCCASH_UPDATE_PRICES,
  BTCCASH_UPDATE_KZT,
  BTCCASH_UPDATE_BTC,
  BTCCASH_SET_FOCUS_BTC,
  BTCCASH_SET_FOCUS_KZT
} from './types'

export const updatePrices = payload => ({
  type: BTCCASH_UPDATE_PRICES,
  payload
})

export const updateKZT = payload => ({
  type: BTCCASH_UPDATE_KZT,
  payload
})

export const updateBTC = payload => ({
  type: BTCCASH_UPDATE_BTC,
  payload
})

export const setFocusKZT = payload => ({
  type: BTCCASH_SET_FOCUS_KZT,
  payload
})

export const setFocusBTC = payload => ({
  type: BTCCASH_SET_FOCUS_BTC,
  payload
})
import {
  BTCCASH_UPDATE_PRICES,
  BTCCASH_UPDATE_KZT,
  BTCCASH_UPDATE_BTC,
  BTCCASH_SET_FOCUS_BTC,
  BTCCASH_SET_FOCUS_KZT
} from "./types"

const initialState = {
  price: 0,
  amountKZT: 0,
  amountBTC: 0.01,
  dates: [],
  rates: [],
  kztFocused: false,
  btcFocused: false
}

let amountBTC
let amountKZT

export default (state = initialState, action) => {
  switch (action.type) {
    case BTCCASH_UPDATE_PRICES:
      amountBTC = state.amountBTC
      amountKZT = state.amountKZT

      if (state.kztFocused) {
        amountBTC = action.payload.btcCashPrice ? state.amountKZT / action.payload.btcCashPrice : 0

        if (amountBTC < 0.00001) {
          amountBTC = 0
        }
      }
      else {
        amountKZT = state.amountBTC * action.payload.btcCashPrice
      }

      return {
        ...state,
        price: action.payload.btcCashPrice,
        dates: action.payload.dates,
        rates: action.payload.btcCashData,
        percent: action.payload.btcCashPercent,
        amountBTC,
        amountKZT
      }
    case BTCCASH_UPDATE_KZT:
      amountBTC = state.price ? action.payload / state.price : 0

      if (amountBTC < 0.00001) {
        amountBTC = 0
      }

      return {
        ...state,
        amountKZT: action.payload,
        amountBTC
      }
    case BTCCASH_UPDATE_BTC:
      return {
        ...state,
        amountBTC: action.payload,
        amountKZT: state.price * action.payload
      }
    case BTCCASH_SET_FOCUS_KZT:
      return {
        ...state,
        kztFocused: action.payload
      }
    case BTCCASH_SET_FOCUS_BTC:
      return {
        ...state,
        btcFocused: action.payload
      }
    default:
      return state
  }
}
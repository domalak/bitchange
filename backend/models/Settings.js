const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    btcusdt: Number,
    usdkzt: Number,
    btckzt: Number,
    kaspiBTCPercent: Number,
    btcKaspiPercent: Number,
    kaspiBTCPrice: Number,
    btcKaspiPrice: Number,
    cashBTCPercent: Number,
    btcCashPercent: Number,
    cashBTCPrice: Number,
    btcCashPrice: Number
})

module.exports = mongoose.model('Settings', schema)

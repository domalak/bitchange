const mongoose = require('mongoose')

const rateSchema = new mongoose.Schema({
    kaspiBTCPrice: Number,
    btcKaspiPrice: Number,
    cashBTCPrice: Number,
    btcCashPrice: Number,
    date: { type: Date, default: Date.now }
})

module.exports = mongoose.model('Rate', rateSchema)
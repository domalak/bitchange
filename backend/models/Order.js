const mongoose = require('mongoose')
const Counter = require('./Counter')

const schema = new mongoose.Schema({
  orderNumber: {
    type: Number
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
    ref: 'User'
  },
  type: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  amountKZT: {
    type: Number,
    required: true
  },
  amountBTC: {
    type: Number,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  btcAddress: {
    type: String,
    required: false
  },
  kaspiCard: {
    type: String,
    required: false
  },
  status: {
    type: String,
    required: false
  }
})

schema.pre('save', async function(next) {
  const name = 'order'

  if (this.isNew) {
    try {
      const counter = await Counter.findOneAndUpdate({ name }, {
        $inc: { value: 1} 
      }, { upsert: true }).exec()
      
      this.orderNumber = counter.value
    } catch (error) {
      return next(error)
    }
  }
  
  return next()
})

const Order = mongoose.model("Order", schema)

module.exports = Order

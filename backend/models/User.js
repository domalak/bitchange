const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  phone: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  idPhoto: {
    type: String,
  },
  selfPhoto: {
    type: String,
  },
  createDate: {
    type: Date,
    default: Date.now
  },
  termsAcceptedDate: {
    type: Date,
    default: Date.now
  },
  phoneVerified: {
    type: Boolean,
    default: false
  },
  smsCode: {
    type: String,
  },
  photoVerifyStatus: {
    type: String
  },
  photoDenyReason: {
    type: String
  },
  socketId: {
    type: String
  },
  isAdmin: {
    type: Boolean,
    default: false
  }
})

const User = mongoose.model('User', schema)

module.exports = User
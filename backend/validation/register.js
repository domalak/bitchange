const Validator = require("validator");
const isEmpty = require("is-empty");
module.exports = function validateRegisterInput(data) {
  let errors = {};
  // При отсутствии введенных параметров переводим их в пустую строку для проверки через валидатор
  data.phone = !isEmpty(data.phone) ? data.phone : "";
  data.password = !isEmpty(data.password) ? data.password : "";

  // Phone checks
  if (Validator.isEmpty(data.phone)) {
    errors.phone = "Телефонный номер не введен";
  }

  //проверка на принадлежность введенного телефона к Казастанским операторам
  let regEx = /^((7)+([0-9]){9})$/
  if (!regEx.test(data.phone)){
    errors.phone = "Телефонный номер введен неправильно"
  }

  // Проверяем введен ли пароль
  if (Validator.isEmpty(data.password)) {
    errors.password = "Пароль не введен";
  }

  // Задаем паролю минимальную длину в 8 символов
  if (!Validator.isLength(data.password, { min: 8 })) {
    errors.password = "Длина пароля должна состовлять от 8 символов";
  }

return {
    errors,
    isValid: isEmpty(errors)
  }
}
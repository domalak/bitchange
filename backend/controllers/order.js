const Order = require('../models/Order')
const Settings = require('../models/Settings')
const { admins } = require('../local')
const { telegram } = require('../telegram')

const newOrderPriceTimeout = 3 * 60 * 1000
const paymentPriceTimeout = 15 * 60 * 1000

const create = async ctx => {
  const order = new Order({
    ...ctx.request.body,
    status: 'new'
  })

  const newOrder = await order.save()
  const { orderNumber, status, type, price, date, amountBTC, amountKZT } = newOrder

  return ctx.body = { orderNumber, status, type, price, date, amountBTC, amountKZT }
}

const get = async ctx => {
  let order
  try {
    order = await Order.findOne({
      orderNumber: ctx.request.query.orderNumber
    })
  } catch (error) {
    return Promise.reject(error)
  }
  
  if ((!order.user || order.user.toString() !== ctx.state.user.id) && !ctx.state.user.data.isAdmin) {
    return ctx.status = 404
  }

  const orderDate = new Date(order.date)

  let modifiedOrder = order

  if (Date.now() - orderDate.getTime() >= paymentPriceTimeout) {
    const settings = await Settings.findOne()

    order.date = Date.now()

    switch (order.type) {
      case 'kaspibtc':
        if (order.price < settings.kaspiBTCPrice) {
          order.price = settings.kaspiBTCPrice
          order.amountBTC = order.amountKZT / order.price
        }
        break
      case 'btckaspi':
        if (order.price > settings.btcKaspiPrice) {
          order.price = settings.btcKaspiPrice
          order.amountKZT = order.amountBTC * order.price
        }
        break
      case 'cashbtc':
        if (order.price < settings.cashBTCPrice) {
          order.price = settings.cashBTCPrice
          order.amountBTC = order.amountKZT / order.price
        }
        break
      case 'btccash':
        if (order.price > settings.btcCashPrice) {
          order.price = settings.btcCashPrice
          order.amountKZT = order.amountBTC * order.price
        }
        break
      default:
        break
    }

    modifiedOrder = await order.save()
  }

  const { price, date, amountBTC, amountKZT, orderNumber, type, btcAddress, kaspiCard, status } = modifiedOrder

  return ctx.body = { price, date, amountBTC, amountKZT, orderNumber, type, btcAddress, kaspiCard, status }
}

const getPrice = async ctx => {
  let order
  try {
    order = await Order.findOne({
      orderNumber: ctx.request.query.orderNumber
    })
  } catch (error) {
    return Promise.reject(error)
  }

  const orderDate = new Date(order.date)

  let modifiedOrder = order

  if (Date.now() - orderDate.getTime() >= newOrderPriceTimeout) {
    const settings = await Settings.findOne()

    order.date = Date.now()

    switch (order.type) {
      case 'kaspibtc':
        if (order.price < settings.kaspiBTCPrice) {
          order.price = settings.kaspiBTCPrice
        }
        break
      case 'btckaspi':
        if (order.price > settings.btcKaspiPrice) {
          order.price = settings.btcKaspiPrice
        }
        break
      case 'cashbtc':
        if (order.price < settings.cashBTCPrice) {
          order.price = settings.cashBTCPrice
        }
        break
      case 'btccash':
        if (order.price > settings.btcCashPrice) {
          order.price = settings.btcCashPrice
        }
        break
      default:
        break
    }

    modifiedOrder = await order.save()
  }

  const { price, date } = modifiedOrder

  return ctx.body = { price, date }
}

const update = async ctx => {
  const { orderNumber, amountKZT, amountBTC, btcAddress, kaspiCard } = ctx.request.body

  const order = await Order.findOne({ orderNumber })

  const orderDate = new Date(order.date)

  if (Date.now() - orderDate.getTime() >= 180000) {
    const settings = await Settings.findOne()

    order.date = Date.now()

    switch (order.type) {
      case 'kaspibtc':
        if (order.price < settings.kaspiBTCPrice) {
          order.price = settings.kaspiBTCPrice
        }
        break
      case 'btckaspi':
        if (order.price > settings.btcKaspiPrice) {
          order.price = settings.btcKaspiPrice
        }
        break
      case 'cashbtc':
        if (order.price < settings.cashBTCPrice) {
          order.price = settings.cashBTCPrice
        }
        break
      case 'btccash':
        if (order.price > settings.btcCashPrice) {
          order.price = settings.btcCashPrice
        }
        break
      default:
        break
    }
  }

  switch (order.type) {
    case 'kaspibtc':
      order.btcAddress = btcAddress
      order.amountKZT = amountKZT
      order.amountBTC = order.price ? order.amountKZT / order.price : 0

      if (order.amountBTC < 0.00001) {
        order.amountBTC = 0
      }

      break
    case 'btckaspi':
      order.kaspiCard = kaspiCard
      order.amountBTC = amountBTC
      order.amountKZT = order.amountBTC * order.price

      break
    case 'cashbtc':
      order.amountKZT = amountKZT
      order.amountBTC = order.price ? order.amountKZT / order.price : 0

      if (order.amountBTC < 0.00001) {
        order.amountBTC = 0
      }

      break
    case 'btccash':
      order.amountBTC = amountBTC
      order.amountKZT = order.amountBTC * order.price

      break
    default:
      break
  }

  order.status = 'receiving_payment'
  order.user = ctx.state.user.id

  const modifiedOrder = await order.save()

  const message = `
Заказ №${order.orderNumber}
Тип заказа: ${order.type}
Сумма KZT: ${order.amountKZT}
Сумма BTC: ${order.amountBTC}
Курс BTC: ${order.price}
Статус: ${order.status}
Адрес BTC: ${order.btcAddress}
Карта Kaspi: ${order.kaspiCard}
  `

  for (const admin of admins) {
    await telegram.sendMessage(admin, message)
  }
  

  return ctx.body = {
    status: modifiedOrder.status
  }
}

const setPaymentSent = async ctx => {
  const orderNumber = ctx.request.body.orderNumber

  let order
  try {
    order = await Order.findOne({
      orderNumber
    })
  } catch (error) {
    return Promise.reject(error)
  }

  if (!order.user || order.user.toString() !== ctx.state.user.id) {
    return ctx.status = 404
  }

  order.status = 'user_sent_payment'

  const modifiedOrder = await order.save()

  const message = `
Заказ №${modifiedOrder.orderNumber}
Тип заказа: ${modifiedOrder.type}
Сумма KZT: ${modifiedOrder.amountKZT}
Сумма BTC: ${modifiedOrder.amountBTC}
Курс BTC: ${modifiedOrder.price}
Статус: ${modifiedOrder.status}
Адрес BTC: ${modifiedOrder.btcAddress}
Карта Kaspi: ${modifiedOrder.kaspiCard}
  `

  for (const admin of admins) {
    await telegram.sendMessage(admin, message, {
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: 'Перевод получен',
              callback_data: `payment_received_${modifiedOrder.orderNumber}`
            },
            {
              text: 'Перевода нет',
              callback_data: `payment_not_received_${modifiedOrder.orderNumber}`
            }
          ]
        ]
      }
    })
  }

  return ctx.body = {
    status: modifiedOrder.status
  }
}

const getOrdersHistory = async ctx => {
  const user = ctx.request.body.user
  
  let orders = await Order.find({ user }).sort({ orderNumber: -1 })
  
  ctx.status = 200
  
  return ctx.body = { orders }
  
}

module.exports = {
  create,
  getPrice,
  get,
  update,
  setPaymentSent,
  getOrdersHistory
}
const User = require('../models/User')
const Order = require('../models/Order')
const path = require('path')
const fs = require('fs')

const getPhoto = (photo, type) => {
  return fs.createReadStream(path.join(__dirname, `../public/uploadedImages/${photo}_${type}Photo.jpg`))
}

const adminGetUsers = async ctx => {
  const users = await User.find().sort({ createDate: -1 })
  ctx.status = 200
  return ctx.body = { users }
}

const adminGetOrders = async ctx => {
  const orders = await Order.find()

  return ctx.body = { orders }
}

const adminGetPhoto = async function (ctx) {
  return ctx.body = getPhoto(ctx.params.phone, ctx.params.type)
}

const adminGetUserInfo = async function (ctx) {
  const { phone, phoneVerified, smsCode, photoVerifyStatus, isAdmin, createDate } = await User.findOne({ phone: ctx.params.phone })
  return ctx.body = {
    phone,
    phoneVerified,
    smsCode,
    photoVerifyStatus,
    isAdmin,
    createDate
  }
}

const adminChangeStatus = async function (ctx) {
  const newStatus = ctx.request.body.selectedStatus
  const newDenyReason = ctx.request.body.denyReason

  const user = await User.findOne({ phone: ctx.request.body.phone })

  if (newStatus) {
    user.photoVerifyStatus = newStatus
  }
  if (newDenyReason) {
    user.denyReason = newDenyReason
  }
  user.save()
  ctx.status = 200
}

const shouldBeAdmin = (ctx, next) => {
  if (ctx.state.user.data.isAdmin) {
    return next()
  }

  return ctx.status = 404
}

module.exports = {
  adminGetUsers,
  adminGetOrders,
  adminGetPhoto,
  adminGetUserInfo,
  adminChangeStatus,
  shouldBeAdmin
}
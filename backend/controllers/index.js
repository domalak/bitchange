const getHours = async ctx => {
    const currentTime = new Date()
    if(currentTime.getHours() >= 10 && currentTime.getHours() < 22){
        ctx.status = 200
        return ctx.body = {
            message: 'Site is working'
        }
    }
    ctx.status = 503
    return ctx.body = { message: 'Site is offline' }
}
  
  
module.exports = {
  getHours
}
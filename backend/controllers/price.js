const rp = require('request-promise')
const util = require('util')
const xml2js = require('xml2js')
const winston = require('winston')
const logger = winston.createLogger({
  transports: [new winston.transports.Console()],
  format: winston.format.simple()
})

const Rate = require('../models/Rate')
const Settings = require('../models/Settings')

function getBTCPrice(io) {
  return rp({
    uri: 'https://api.binance.com/api/v3/ticker/price',
    qs: {
      symbol: 'BTCUSDT'
    }
  })
    .then(async function (response) {
      const btcUSD = JSON.parse(response)
      let rec
      try {
        rec = await Settings.findOneAndUpdate({}, {
          btcusdt: Number(btcUSD.price)
        }, { upsert: true }).exec()
      } catch (error) {
        logger.log('error', util.inspect(error))
        return Promise.reject()
      }

      rec.kaspiBTCPrice = Number(rec.btcusdt * rec.usdkzt * (100 + rec.kaspiBTCPercent) / 100)
      rec.btcKaspiPrice = Number(rec.btcusdt * rec.usdkzt * (100 + rec.btcKaspiPercent) / 100)
      rec.cashBTCPrice = Number(rec.btcusdt * rec.usdkzt * (100 + rec.cashBTCPercent) / 100)
      rec.btcCashPrice = Number(rec.btcusdt * rec.usdkzt * (100 + rec.btcCashPercent) / 100)

      try {
        await rec.save()
      } catch (error) {
        logger.log('error', util.inspect(error))
        return Promise.reject()
      }

      const rate = new Rate({
        kaspiBTCPrice: rec.kaspiBTCPrice,
        btcKaspiPrice: rec.btcKaspiPrice,
        cashBTCPrice: rec.cashBTCPrice,
        btcCashPrice: rec.btcCashPrice
      })

      try {
        await rate.save()
      } catch (error) {
        logger.log('error', util.inspect(error))
        return Promise.reject()
      }

      return sendData(io)
    })
    .catch(function (err) {
      logger.log('error', err.message)
      return Promise.reject()
    })
}

function getUSDPrice() {
  return rp('http://www.nationalbank.kz/rss/rates_all.xml')
    .then(function (resp) {
      const parser = new xml2js.Parser({ explicitArray: false })

      parser.parseString(resp, async function (_err, x) {
        for (let i = 0; i < x.rss.channel.item.length; i++) {
          if (x.rss.channel.item[i].title === 'USD') {

            try {
              await Settings.updateOne({}, {
                usdkzt: Number(x.rss.channel.item[i].description)
              }, { upsert: true })
            } catch (error) {
              logger.log('error', util.inspect(error))
              return Promise.reject()
            }


            return Promise.resolve()
          }
        }
      })
    })
    .catch(function (err) {
      logger.log('error', util.inspect(err))
      return Promise.reject(err)
    })
}

async function sendData(io) {
  let rec
  try {
    rec = await Settings.findOne({}).exec()
  } catch (error) {
    logger.log('error', util.inspect(error))
    return Promise.reject()
  }

  let data
  try {
    data = await Rate.find({}).sort({date: -1}).limit(60).exec()
  } catch (error) {
    logger.log('error', util.inspect(error))
    return Promise.reject()
  }

  data.reverse()

  const kaspiBTCData = data.map(el => el.kaspiBTCPrice)
  const btcKaspiData = data.map(el => el.btcKaspiPrice)
  const cashBTCData = data.map(el => el.cashBTCPrice)
  const btcCashData = data.map(el => el.btcCashPrice)
  const dates = data.map(el => `${el.date.toTimeString().substring(0, el.date.toTimeString().lastIndexOf(':'))}`)
  

  io.emit('price', {
    kaspiBTCPrice: rec.kaspiBTCPrice,
    btcKaspiPrice: rec.btcKaspiPrice,
    cashBTCPrice: rec.cashBTCPrice,
    btcCashPrice: rec.btcCashPrice,
    kaspiBTCPercent: rec.kaspiBTCPercent,
    btcKaspiPercent: rec.btcKaspiPercent,
    cashBTCPercent: rec.cashBTCPercent,
    btcCashPercent: rec.btcCashPercent,
    kaspiBTCData,
    btcKaspiData,
    cashBTCData,
    btcCashData,
    dates
  })
}

module.exports = {
  getBTCPrice,
  getUSDPrice,
  sendData
}
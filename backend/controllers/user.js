const rp = require('request-promise')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const winston = require('winston')
const util = require('util')
const logger = winston.createLogger({
  transports: [new winston.transports.Console()],
  format: winston.format.simple()
})
const fs = require('fs')

//smsVerifcation login and password export:
const { smsLogin, smsPassword } = require('../local')

const path = require('path')
const koabody = require('koa-body')({
  multipart: true,
    formidable: {
      uploadDir: path.join(__dirname, '../public/uploadedImages'),
      keepExtensions: true,
      onFileBegin:(_name, file) => {
        const dir = path.join(__dirname,`../public/uploadedImages`)
        file.path = `${dir}/${file.name}.jpg`
      },
    },
    urlencoded: true
})
const validateRegisterInput = require('../validation/register')
// const validateLoginInput = require('../validation/login')

const User = require('../models/User')
const keys = require('../config/keys')

const { admins } = require('../local')
const { telegram } = require('../telegram')

const expireTime = process.env.NODE_ENV === 'production' ? 60 * 60 * 24 * 14 : 60 * 60 * 12

const register = async (ctx) => {
  const {
    errors,
    isValid
  } = validateRegisterInput(ctx.request.body)

  if (!isValid) {
    ctx.status = 400
    return ctx.body = {
      errors
    }
  }

  try {
    const user = await User.findOne({
      phone: ctx.request.body.phone
    })

    if (user) {
      ctx.status = 400
      errors.phone = 'Данный телефонный номер уже зарегистрирован'
      return ctx.body = {
        errors
      }
    }

    let newUser = new User({
      phone: ctx.request.body.phone,
      password: ctx.request.body.password
    })

    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash(newUser.password, salt)
    newUser.password = hash

    //generating random string of 6 numbers
    const number = Math.floor(100000 + Math.random() * 900000)
    let smsCode = number.toString(10)
    newUser.smsCode = smsCode

    newUser = await newUser.save()

    const phoneInFrmt = '+7' + newUser.phone
    //using Smsc.KZ API
    const message = `Ваш код верификации на Bitchange: ${smsCode}. https://bitchange.club/verification`

    if (process.env.NODE_ENV === 'production') {
      rp({
        uri: `https://smsc.kz/sys/send.php`,
        qs: {
          login: smsLogin,
          psw: smsPassword,
          phones: phoneInFrmt,
          mes: message,
          fmt: 3,
          cost: 3
        }
      }).then(response => logger.log('info', response))
        .catch(err => logger.log('error', err))
    }

    const payload = {
      phone: newUser.phone,
      phoneVerified: newUser.phoneVerified,
      photoVerifyStatus: newUser.photoVerifyStatus
    }

    const signPromise = new Promise((resolve, reject) => {
      jwt.sign({
        id: newUser.id
      }, keys.secretOrKey, {
        expiresIn: expireTime
      }, (err, token) => {
        if (err) {
          return reject(err)
        }

        return resolve(token)
      })
    })

    const token = await signPromise

    payload.token = token

    return ctx.body = payload
  } catch (error) {
    logger.log('error', util.inspect(error))
    return ctx.status = 500
  }
}

const login = (ctx) => {
  let errors = {}
  // const {errors, isValid} = validateLoginInput(ctx.request.body);
  
  // if(!isValid){
  //     ctx.status = 400
  //     return ctx.body = { errors }
  // }

  const phone = ctx.request.body.user.phone
  const password = ctx.request.body.user.password
  return User.findOne({
      phone
    })
    .then(user => {
      if (!user) {
        ctx.status = 400
        errors.phoneNotFound = 'Данный номер телефона не зарегистрирован'
        return ctx.body = {
          errors
        }
      }

      return bcrypt.compare(password, user.password).then(isMatch => {
        if (isMatch) {
          const payload = {
            id: user.id
          }

          const signPromise = new Promise((resolve, reject) => {
            jwt.sign(payload, keys.secretOrKey, {
              expiresIn: expireTime
            }, (err, token) => {
              if (err) {
                return reject(err)
              }

              ctx.body = {
                token,
                phone: user.phone,
                phoneVerified: user.phoneVerified,
                photoVerifyStatus: user.photoVerifyStatus,
                photoDenyReason: user.photoDenyReason,
                isAdmin: user.isAdmin
              }

              return resolve(ctx.status = 200)
            })
          })
          return signPromise
        }
        ctx.status = 400
        errors.passwordIncorrect = 'Пароль введен неверно'
        return ctx.body = {
          errors
        }

      })
    })
}

const get = async ctx => {
  try {
    let user
    if (ctx.state.user.data.isAdmin) {
      user = await User.findOne({
        phone: ctx.request.query.phone
      })
    }
    else {
      user = await User.findById(ctx.state.user.id)

      user.socketId = ctx.request.query.socketId

      await user.save()
    }

    

    const {
      phone,
      id,
      phoneVerified,
      photoVerifyStatus,
      photoDenyReason,
      isAdmin
    } = user

    return ctx.body = {
      phone,
      id,
      phoneVerified,
      photoVerifyStatus,
      photoDenyReason,
      isAdmin
    }
  } catch (error) {
    return ctx.status = 500
  }
}

const authorized = async (ctx, next) => {
  const verifyPromise = new Promise((resolve, reject) => {
    jwt.verify(ctx.header.authorization, keys.secretOrKey, (err, payload) => {
      if (err) {
        return reject(err)
      }

      return resolve(payload)
    })
  })


  try {
    ctx.state.user = await verifyPromise

    ctx.state.user.data = await User.findOne({ _id: ctx.state.user.id })

    return next()
  } catch (error) {
    return ctx.status = 401
  }
}

const verify = async ctx => {
  let errors = {}

  const user = await User.findById(ctx.state.user.id)

  if (ctx.request.body.smsCode === user.smsCode) {
    user.phoneVerified = true
    await user.save()
    return ctx.body = {
      phoneVerified: true
    }
  }
  
  ctx.status = 400
  errors.smsCode = "Номер введен неверно"
  return ctx.body = {
    errors
  }
  
}

const verifyPhoto = async ctx => {
  let user = await User.findOne({ _id: ctx.state.user.id })
  user.photoVerifyStatus = 'processing'
  user.idPhoto = `${user.phone}_idPhoto.jpg`
  user.selfPhoto = `${user.phone}_selfPhoto.jpg`
  await user.save()

  for (const admin of admins) {
    await telegram.sendPhoto(admin, { source: fs.createReadStream(path.join(__dirname, `../public/uploadedImages/${user.idPhoto}`)) })
    await telegram.sendPhoto(admin, { source: fs.createReadStream(path.join(__dirname, `../public/uploadedImages/${user.phone}_idBackPhoto.jpg`)) })
    await telegram.sendPhoto(admin, { source: fs.createReadStream(path.join(__dirname, `../public/uploadedImages/${user.selfPhoto}`)) })
  }

  ctx.status = 200
  return ctx.body = {
    photoVerifyStatus: 'processing'
  }
}

module.exports = {
  register,
  login,
  authorized,
  get,
  verify,
  verifyPhoto,
  koabody
}
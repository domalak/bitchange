const http = require('http')
const Koa = require('koa')
const app = new Koa()
const server = http.createServer(app.callback())
const io = require('socket.io')(server)
const util = require('util')
const bodyparser = require('koa-bodyparser')
const winston = require('winston')
const cors = require('@koa/cors')
const Telegraf = require('telegraf')
const { botToken } = require('./local')
const Order = require('./models/Order')
const User = require('./models/User')

const logger = winston.createLogger({
  transports: [new winston.transports.Console()],
  format: winston.format.simple()
})

logger.log('info', `NODE_ENV set to: ${process.env.NODE_ENV}`)

const mongoose = require('mongoose')
mongoose.connect("mongodb://127.0.0.1:27017/bitcoinkz", {
  useNewUrlParser: true,
  useFindAndModify: false
}).then(() => {
  logger.log('info', 'Mongo connected')
})

//cors
app.use(cors())

//bodyparser
app.use(bodyparser())

//Api для модулей конфигурации пользователей
const { routes, allowedMethods } = require('./routes')

app.use(routes())
app.use(allowedMethods())

app.use((ctx, next) => {
  ctx.state.io = io

  return next()
})

const priceController = require('./controllers/price')
const port = 9000
const btcUpdateTime = 30 * 1000
const usdUpdateTime = 60 * 60 * 1000

io.on('connection', socket => {
  priceController.sendData(socket)
})

priceController.getUSDPrice()
  .then(() => priceController.getBTCPrice(io))
  .then(() => {
    setInterval(async () => {
      try {
        await priceController.getUSDPrice()
      } catch (error) {
        logger.log('error', util.inspect(error))
        return Promise.reject()
      }
    }, usdUpdateTime)

    setInterval(async () => {
      try {
        await priceController.getBTCPrice(io)
      } catch (error) {
        logger.log('error', util.inspect(error))
        return Promise.reject()
      }
    }, btcUpdateTime)

    logger.log('info', `Prices are initialized`)
  })
  .catch(function (err) {
    logger.log('error', util.inspect(err))
    return Promise.reject()
  })

server.listen(port, () => {
  logger.log('info', `Server is listening on port ${port}`)
})

const bot = new Telegraf(botToken)

bot.start(ctx => {
  logger.log('info', `New user: ${JSON.stringify(ctx.from)}`)
  return ctx.reply('Салам!')
})

bot.on('callback_query', async ctx => {
  const { callbackQuery } = ctx

  const orderNumberRegExp = /_(\d+)$/

  const orderNumber = callbackQuery.data.match(orderNumberRegExp)[1]

  const queryType = callbackQuery.data.replace(orderNumberRegExp, '')

  const order = await Order.findOne({ orderNumber })

  switch (queryType) {
    case 'payment_received':
      order.status = 'sending_payment'
      break
    case 'payment_not_received':
      order.status = 'receiving_payment'
      break
    default:
      return ctx.reply('Я ничего не сделал')
  }

  const modifiedOrder = await order.save()
  
  const user = await User.findOne({ _id: modifiedOrder.user })
  
  io.to(user.socketId).emit('order', {
    status: modifiedOrder.status
  })

  return ctx.reply(`Статус изменен на ${modifiedOrder.status}`)
})

bot.launch()
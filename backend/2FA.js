const speakeasy = require('speakeasy')
const inquirer = require('inquirer')

const secret = speakeasy.generateSecret({ length: 10 })

console.log(secret);


return inquirer.prompt([
  {
    type: 'input',
    name: 'code',
    message: "Enter code:"
  }
])
  .then(res => {
    const verified = speakeasy.totp.verify({
      secret: secret.base32,
      encoding: 'base32',
      token: res.code
    })

    console.log(verified)
  })
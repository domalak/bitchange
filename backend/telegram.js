const Telegram = require('telegraf/telegram')
const { botToken } = require('./local')

const telegram = new Telegram(botToken)

module.exports = {
  telegram
}
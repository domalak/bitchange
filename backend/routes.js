const Router = require('koa-router')
const router = new Router()
const userController = require('./controllers/user')
const orderController = require('./controllers/order')
const defaultController = require('./controllers/index')
const adminController = require('./controllers/admin')
//api route: 
// routes/api/users/register

router.get('/user', userController.authorized, userController.get)
router.post('/register', userController.register)
router.post('/login', userController.login)
router.post('/verification', userController.authorized, userController.verify)
router.post('/upload', userController.authorized, userController.koabody, userController.verifyPhoto)
router.post('/orders-history', userController.authorized, orderController.getOrdersHistory)
router.get('/getHours', defaultController.getHours)

//admin routes:
router.get(
  '/adminGetUsers',
  userController.authorized,
  adminController.shouldBeAdmin,
  adminController.adminGetUsers
)
router.get(
  '/adminGetPhoto/:type/:phone',
  userController.authorized,
  adminController.adminGetPhoto
)
router.get(
  '/adminGetUserInfo/:phone',
  userController.authorized,
  adminController.shouldBeAdmin,
  adminController.adminGetUserInfo
)
router.post(
  '/adminChangeStatus',
  userController.authorized,
  adminController.shouldBeAdmin,
  adminController.adminChangeStatus
)
router.get(
  '/adminGetOrders',
  userController.authorized,
  adminController.shouldBeAdmin,
  adminController.adminGetOrders
)

router.post('/order', orderController.create)
router.get('/order-price', userController.authorized, orderController.getPrice)
router.get('/order', userController.authorized, orderController.get)
router.put('/order', userController.authorized, orderController.update)
router.post('/set-payment-sent', userController.authorized, orderController.setPaymentSent)

module.exports = {
  routes: () =>  router.routes(),
  allowedMethods: () =>  router.allowedMethods()
}
// function routes () { return router.routes() }
// export function allowedMethods () { return router.allowedMethods() }